#include <systemc>
#include "during.h"    	
#include <mutex>	
#include <string>

using namespace std;
using namespace sc_core;
/**
* CLASS FIFO_OUT_IF
*/
template <class T>
class fifo_out_if :  virtual public sc_interface
{
public:
  virtual void write(T) = 0 ;          	  // blocking write from a systemC task 
  virtual void write_during(T) = 0 ;      // blocking write from a during  task
  virtual bool nb_write(T) = 0 ;          // non-blocking write
  virtual int num_free() const  = 0;      // free entries
  fifo_out_if(){};
private:
  fifo_out_if (const fifo_out_if&);      // disable copy
  fifo_out_if& operator= (const fifo_out_if&); // disable =
};

/**
* END CLASS FIFO_OUT_IF
*/


/**
* CLASS FIFO_OUT
*/
template <class T>
class fifo_out
: public sc_port<fifo_out_if<T>,0,SC_ONE_OR_MORE_BOUND>
{
public:
    // typedefs
    typedef T                                        data_type;
    typedef fifo_out_if<data_type>                   if_type;
    typedef sc_port<if_type,0,SC_ONE_OR_MORE_BOUND>  base_type;
    typedef fifo_out<data_type>                      this_type;
    typedef if_type                                  out_if_type;
    typedef sc_port_b<out_if_type>                   out_port_type;

public:

    // constructors
  	fifo_out()
	: base_type()
	{}
    explicit fifo_out( const char* name_ )
	: base_type( name_ )
	{}

    explicit fifo_out( out_if_type& interface_ )
	: base_type( interface_ )
	{}

    fifo_out( const char* name_, out_if_type& interface_ )
	: base_type( name_, interface_ )
	{}

    
    // destructor (does nothing)

    virtual ~fifo_out()
	{}


    // interface access shortcut methods

    // blocking write

    void write( const data_type& value_ )
        { (*this)->write( value_ ); }

    void write_during( const data_type& value_ )
        { (*this)->write_during( value_ ); }

    // non-blocking write

    bool nb_write( const data_type& value_ )
        { return (*this)->nb_write( value_ ); } 


    // get the number of free spaces

    int num_free() const
        { return (*this)->num_free(); }

private:
    // disabled
    fifo_out( const this_type& );
    this_type& operator = ( const this_type& );
};

/**
* END CLASS FIFO_OUT
*/

/**
* CLASS FIFO_IN_IF
*/
template <class T>
class fifo_in_if :  virtual public sc_interface
{
public:
  virtual void read(T&) = 0;	         // blocking read from a systemC task
  virtual void read_during(T&) = 0;	 // blocking read from a during  task
  virtual bool nb_read(T&) = 0;          // nb_blocking read
  virtual T read() = 0;
  virtual int num_available() const = 0; // available

protected:
  fifo_in_if(){};
private:
  fifo_in_if(const fifo_in_if&);             // disable copy
  fifo_in_if& operator= (const fifo_in_if&); // disable =
};
/**
* END CLASS FIFO_IN_IF
*/

template <class T>
class fifo_in
: public sc_port<fifo_in_if<T>,0,SC_ONE_OR_MORE_BOUND>
{
public:
    // typedefs
    typedef T                                       data_type;
    typedef fifo_in_if<data_type>                   if_type;
    typedef sc_port<if_type,0,SC_ONE_OR_MORE_BOUND> base_type;
    typedef fifo_in<data_type>                      this_type;
    typedef if_type                                 in_if_type;
    typedef sc_port_b<in_if_type>                   in_port_type;

public:

    // constructors

    fifo_in()
	: base_type()
	{}

    explicit fifo_in( const char* name_ )
	: base_type( name_ )
	{}

    explicit fifo_in( in_if_type& interface_ )
	: base_type( interface_ )
	{}

    explicit fifo_in( in_port_type& parent_ )
	: base_type( parent_ )
	{}

    fifo_in( this_type& parent_ )
	: base_type( parent_ )
	{}


    // destructor (does nothing)

    virtual ~fifo_in()
	{}


    // interface access shortcut methods

    // blocking read


    void read_during( data_type& value_ )
        { (*this)->read_during( value_ ); }

    void read( data_type& value_ )
        { (*this)->read( value_ ); }

    bool nb_read( data_type& value_ )
        { return (*this)->nb_read( value_ ); }

    data_type read()
        { return (*this)->read(); }


    // get the number of available samples

    int num_available() const
        { return (*this)->num_available(); }

private:
    // disabled
    fifo_in( const this_type& );
    this_type& operator = ( const this_type& );
};



/**
* CLASS FIFO
*/
template <class T>
class fifo
: public fifo_in_if<T>,
  public fifo_out_if<T>,
  public sc_prim_channel,
  public sc_during
{
protected:
  int size;                 // size
  T* buf;                   // fifo buffer
  int free;                 // free space
  int ri;                   // read index
  int wi;                   // write index
  int num_readable;
  int num_read;
  int num_written;

  mutex fifo_mutex;


  sc_event ecriture;
  sc_event lecture;

public:
  // constructor
  fifo(int size_ = 16): sc_prim_channel(sc_gen_unique_name("myfifo"))
  {
    size = size_;
    buf = new T[size];
    reset();
  }
  int num_available() const
  {
    return num_readable - num_read;
  }

  int num_free() const
  {
    return size - num_readable - num_written;
  }

  void write(T c)        // blocking write from a systemC task
  {

	unique_lock<mutex> lock(fifo_mutex); 
	while (num_free() <= 0) {
		lock.unlock();
		//wait(1,SC_NS);
		wait(lecture);
		lock.lock();
	}
	num_written++;
	buf[wi] = c;
	wi = (wi + 1) % size;
	free--;
	update();

	lock.unlock();
	ecriture.notify();

  }

  void write_during(T c)        // blocking write from a during task
  {

	unique_lock<mutex> lock(fifo_mutex); 
	while (num_free() <= 0) {
		/*lock.unlock();
		extra_time(1, SC_NS);
		catch_up();
		lock.lock();*/

		lock.unlock();
		sc_call([this]{wait(lecture);});
		lock.lock();


	}
	num_written++;
	buf[wi] = c;
	wi = (wi + 1) % size;
	free--;
	update();

	lock.unlock();
	sc_call([this]{ecriture.notify();});
  }

  bool nb_write(T c)        // non blocking write
  {
	bool res=true;
	unique_lock<mutex> lock(fifo_mutex); 
	if (num_free() <= 0) {
		res = false;
		lock.unlock();
	} else {
		num_written++;
		buf[wi] = c;
		wi = (wi + 1) % size;
		free--;
		update();
		lock.unlock();
		ecriture.notify();
	}


	return res;
  }
	
  void reset()
  {
    free = size;
    ri = 0;
    wi = 0;
	num_readable=0;
	num_read=0;
	num_written=0;
  }

  void read(T& c)        // blocking read callable from a systemC task
  {
	unique_lock<mutex> lock2(fifo_mutex); 
	while (num_available() <= 0) {
		lock2.unlock();
		//wait(1,SC_NS);
		wait(ecriture);
		lock2.lock();
	}
	num_read++;
	c = buf[ri];
	ri = (ri + 1) % size;
	free++;
	update();
	lock2.unlock();
	lecture.notify();
  } 

  void read_during(T& c)        // blocking read callable from a during task
  {
	unique_lock<mutex> lock2(fifo_mutex); 
	while (num_available() <= 0) {
		/*lock2.unlock();
		extra_time(1, SC_NS);
		catch_up();
		lock2.lock();*/
		lock2.unlock();		
		sc_call([this]{wait(1,SC_NS);});		
		lock2.lock();
	}
	num_read++;
	c = buf[ri];
	ri = (ri + 1) % size;
	free++;
	update();
	sc_call([this]{lecture.notify();});		
	
  } 

  bool nb_read(T& c)        // non blocking read
  {
	bool res = true;
	unique_lock<mutex> lock2(fifo_mutex); 
	if (num_available() <= 0) {
		res=false;
		lock2.unlock();
	} else {
		num_read++; 
		c = buf[ri];
		ri = (ri + 1) % size;
		free++;
		update();
		lock2.unlock();
		lecture.notify();
	}
	return res;
  } 

  void update()
  {    
    num_readable = size - free;
    num_read = 0;
    num_written = 0;
  }

  T read()                // shortcut read function
  {
    T c;
    read(c);
    return c;
  }

  ~fifo()                   //destructor
  {
    delete [] buf;
  }
};
