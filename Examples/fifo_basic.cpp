/********************************************************************
 * Copyright (C) 2014 by Verimag                                    *
 * Initial author: Sancassa Gaëtan                                  *
 * This file must not be distributed outside Verimag                *
 ********************************************************************/

/*!
  The most important aspect in this example is the synchronisation between SystemC and sc_during
	--> spécialement une lecture ne doit pas se faire tant qu'il n'y a pas d'écriture


En écrivant dans un module SystemC et en lisant dans un module SC_DURING :
lecture dans une boucle infini à l'aide d'un "during"
A: ecrire (4 ns):100
B: read (4 ns): FIFO EMPTY
B: read (5 ns): Received 100

	Si écriture au temps t lecture possible   au temps t+1;
	Si écriture au temps t lecture impossible au temps t  ;
	
	Première approche : 
	sc_call(wait(1));
		--> si la lecture échoue, on fait avancer le temps de systemC (module d'écriture), ainsi la lecture sur la FIFO se fera 
		Résultat : 
A: ecrire (3 ns):100
B: read_boucle (3 ns): Received 100
Lecture FIFO impossible (4 ns)
// TEST-IGNORE: time[] = 1.3325 ms real, 1.0635 ms CPU, 544.835 us CPU (this thread).
		Pourquoi ici, la lecture peut se réaliser au temps de l'écriture ?
		Hypothèse : le sc_call(wait(1)) va augmenter le temps de systemC, validant l'écriture du temps 3 ns, autorisant ainsi la lecture

	Seconde approche : 
	Appele d'une fonction de lecture non bloquante sur la sc_fifo. si rien à lire alors appele d'une fonction bloquante
	Resultat : Fonctionne comme attendu

 Lecture FIFO impossible (1 ns)
A: ecrire (3 ns):100
B: read_boucle (3 ns): Received 100
 Lecture FIFO impossible (4 ns)
// TEST-IGNORE: time[] = 1.41709 ms real, 1.36518 ms CPU, -4.16189e+06 ns CPU (this thread).


	Troisième approche : 
	extra_time(1), catch_up()
		--> si la lecture échou au temps t on suppose que l'écriture à eu lieu soit au temps t soit plus tard du coup extra_time(1);
		le catch_up sert à attendre la fin courante de la tâche de systemC.
		La différence avec la seconde approche est dans l'attente du module de systemC avec ce catch_up().
		Resultat : Fonctionne bien, semble être moins coûteux que les deux approches
A: ecrire (3 ns):100
 Lecture FIFO impossible (3 ns)
B: read_boucle (4 ns): Received 100
// TEST-IGNORE: time[] = 1.11654 ms real, 927.083 us CPU, 432.006 us CPU (this thread).



*/
#define NUMAPPROCHE1
#include <systemc>
#include "during.h"    	

using namespace std;
using namespace sc_core;


void eat_cpu(void) {
	for (int i = 0; i < 10000000; ++i)
		for (int j = 0; j < 5540; ++j)
			continue;
}

SC_MODULE(A)
{
	sc_fifo_out<int> output;
	void ecrire() {
		int val = 100;
		for(;;){
			wait(3, SC_NS);
			eat_cpu();
			output.write(val);
			L_COUT << "A: ecrire (" << sc_time_stamp() << "):" << val << endl;
			val++;
			eat_cpu();
		}
	}
	

	SC_CTOR(A) {
		SC_THREAD(ecrire);
   	}
	
};

SC_MODULE(B), sc_during{
	sc_fifo_in<int> input;

	void lire() {
		for(;;) {
			during(1, SC_NS, [this] { read_boucle(); });
		}
	}

	
	void read_boucle() {
		int val;

#ifdef NUMAPPROCHE1
 		while (!input.nb_read(val)) {
			std::cout << " Lecture FIFO impossible (" << sc_time_stamp() << ")" <<  std::endl;	
			sc_call([this]{
					wait(1, SC_NS);
						});
	 	}
#endif


#ifdef NUMAPPROCHE2
 		while (!input.nb_read(val)) {
			std::cout << " Lecture FIFO impossible (" << sc_time_stamp() << ")" <<  std::endl;	
			extra_time(1, SC_NS); 
			catch_up();
	 	}
#endif

 		std::cout << "B: read_boucle (" << sc_time_stamp() << "): Received " << val << std::endl;
	};	


	SC_CTOR(B) {
		SC_THREAD(lire);
   	}
};
int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	sc_fifo<int> fifo("FIFO",50);

	A a("a");
	B b("b");
	a.output(fifo);
	b.input(fifo);
	{
		measure_time t;
		sc_start(20,SC_NS);
	}

	return 0;
}
