#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <iostream>
#include <unistd.h>
#include <limits.h>

#include "during.h"

#include <time.h>

using namespace std;
using namespace sc_core;

int *slice_numbers;
int *slice_processed;

pthread_mutex_t slice_processed_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t xmutex = PTHREAD_MUTEX_INITIALIZER;

/* command line options */
int auto_loop = 0;
int auto_wait = 0;
int number_of_slices = 20;
int number_of_threads = 1;

/* hardcoded parameters */
const int width = 640;
const int height = 480;
const int rect_height = 10;

//int max_iteration = INT_MAX;
int max_iteration = 3000;
long double x_min = -2;
long double x_max = 1;
long double y_min = -1.3;
long double y_max = 1.3;

/* Portion of a slice to be drawn at once */
struct rect {
	int slice_number;
	int y_start;
};

struct rect rect_to_draw;

Display *display;
Window window;
int screen;
int depth;
GC gc;
char *buffer;
XImage *image;
Colormap cmap;
int color_table[256];

void init_x()
{
	static XSizeHints size_hints;
	Visual *visual;
	Window rootwin;
	XGCValues values;
	unsigned long valuemask = 0;
	display = XOpenDisplay(NULL);

	if (display == NULL) {
		fprintf(stderr, "Failed to open Display!");
		exit(1);
	}

	screen = DefaultScreen(display);
	depth = DefaultDepth(display, screen);
	visual = DefaultVisual(display, screen);
	cmap = DefaultColormap(display, screen);
	rootwin = RootWindow(display, screen);
	window = XCreateSimpleWindow(display, rootwin, 10, 10, width, height, 5,
				     BlackPixel(display, screen),
				     BlackPixel(display, screen));

	size_hints.flags = PSize | PMinSize | PMaxSize;
	size_hints.min_width = width;
	size_hints.max_width = width;
	size_hints.min_height = height;
	size_hints.max_height = height;

	XSetWindowColormap(display, window, cmap);

	//        init_colormap();

	gc = XCreateGC(display, window, valuemask, &values);

	XSetStandardProperties(display, window, "Mandelbrot", "Mandelbrot",
			       None, 0, 0, &size_hints);

	XSelectInput(display, window, ButtonPressMask | KeyPressMask);
	XMapWindow(display, window);

	buffer = 0;

	image =
	    XCreateImage(display, visual, depth, ZPixmap, 0, buffer, width,
			 height, 8, 0);
	{
		// tricks to have it working in non 8-bit depth
		int imgsize = image->height * image->bytes_per_line;
		buffer = (char *)malloc(imgsize);
		int i;
		for (i = 0; i < imgsize; i++)
			buffer[i] = 0;
	}
	image->data = buffer;

	XPutImage(display, window, gc, image, 0, 0, 0, 0, width, height);
}

void Xdraw_rect(struct rect j)
{
	// printf("Drawing rectangle %d,%d\n", j.slice_number, j.y_start);
	int slice_number = j.slice_number;
	int y_start = j.y_start;
	int x_start = (width * slice_number) / number_of_slices;
	int x_end = (width * (slice_number + 1)) / number_of_slices;

	XPutImage(display, window, gc, image,
		  x_start, y_start,
		  x_start, y_start, x_end - x_start, rect_height);

	/* Pas strictement nécessaire, mais si on utilise X de manière
	 * asynchrone, on ne voit pas le bénéfice d'avoir un thread
	 * séparé pour X, puisque c'est la xlib qui travaille en
	 * producteur-consommateur avec un buffer. */
	XSync(display, 0);
}

void draw_screen()
{
	pthread_mutex_lock(&xmutex);
	XPutImage(display, window, gc, image, 0, 0, 0, 0, width, height);

	/* Not necessary strictly speaking, but without this XSync,
	   the xlib already acts as a producer/consumer with a
	   buffer, so the effect of doing a producer/consumer
	   ourserves is not visible. */
	XSync(display, 0);
	pthread_mutex_unlock(&xmutex);
}

void clear_screen()
{
	int x;
	int y;
	for (x = 0; x < width; x++) {
		for (y = 0; y < height; y++) {
			XPutPixel(image, x, y, 0x505050);
		}
	}
	draw_screen();
}

void coordinate_to_long_double(int x, int y, long double *xf, long double *yf)
{
	*xf = (long double)x;
	*yf = (long double)y;
	*xf = (*xf / width) * (x_max - x_min) + x_min;
	*yf = (*yf / height) * (y_max - y_min) + y_min;
}

int iteration_color(int iteration)
{
	iteration <<= 2;
	while (1) {
		if (iteration <= 0xFF) {
			return iteration;
		}
		iteration -= 0xFF;
		if (iteration <= 0xFF) {
			return (iteration << 8) + (0xFF - iteration);
		}
		iteration -= 0xFF;
		if (iteration <= 0xFF) {
			return (iteration << 16) + ((0xFF - iteration) << 8);
		}
		iteration -= 0xFF;
		if (iteration <= 0xFF) {
			return ((0xFF - iteration) << 16);
		}
		iteration -= 0xFF;
	}
}

unsigned long mandelbrot_pixel(int x_int, int y_int)
{
	long double x0, y0, x, y;
	int iteration = 0;

	coordinate_to_long_double(x_int, y_int, &x0, &y0);
	x = x0;
	y = y0;

	while (x * x + y * y <= 4.0 && iteration++ < max_iteration) {
		long double xtemp = x * x - y * y + x0;
		y = 2 * x * y + y0;
		x = xtemp;
	}
	if (iteration >= max_iteration) {
		return 0;
	} else {
		//printf("iteration=%d\n", iteration);
		return iteration_color(iteration);
	}
}

void ask_draw_rect(struct rect j)
{
	pthread_mutex_lock(&xmutex);
	Xdraw_rect(j);
	pthread_mutex_unlock(&xmutex);
}

void draw_slice(int slice_number)
{
	int x, y;
	for (y = 0; y < height; y++) {
		for (x = (width * slice_number) / number_of_slices;
		     x < (width * (slice_number + 1)) / number_of_slices; x++) {
			XPutPixel(image, x, y, mandelbrot_pixel(x, y));
		}
		if (y % rect_height == rect_height - 1) {
			struct rect j;
			j.slice_number = slice_number;
			j.y_start = y - (y % rect_height);
			ask_draw_rect(j);
		}
	}
}

void init()
{
	int i;
	slice_numbers = new int[number_of_slices];
	slice_processed = new int[number_of_slices];
	for (i = 0; i < number_of_slices; i++)
		slice_processed[i] = 0;

	init_x();
}

/***************************************************/
/***************************************************/
/***************************************************/
/***************************************************/
/***************************************************/

struct prodcons_sol : sc_core::sc_module, sc_during
{

	void drawer_process();

	void draw_slice_thread_shop();

	SC_CTOR(prodcons_sol) {
		SC_THREAD(drawer_process);
	}
};

void prodcons_sol::drawer_process()
{
	during(100.0, SC_NS, [this] { draw_slice_thread_shop(); });
}

void prodcons_sol::draw_slice_thread_shop()
{
	int i;
	while (1) {
		pthread_mutex_lock(&slice_processed_mutex);
		for (i = 0; i < number_of_slices; i++)
			if (slice_processed[i] == 0)
				break;
		if (i == number_of_slices) {
			pthread_mutex_unlock(&slice_processed_mutex);
			return;
		}
		printf("Processing slice %d\n", i);
		slice_processed[i] = 1;
		pthread_mutex_unlock(&slice_processed_mutex);
		draw_slice(i);
	}
}

int sc_main(int argc, char *argv[])
{

	int i;

	for (i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "--wait"))
			auto_wait = 1;
		else if (!strcmp(argv[i], "--nb-slices")) {
			i++;
			number_of_slices = atoi(argv[i]);
		} else {
			printf("ERROR: Unknown option %s\n", argv[i]);
			exit(1);
		}
	}

	vector<prodcons_sol *> vect(number_of_slices);
	char s[500];
	for(unsigned int i = 0; i < vect.size(); i++) {
		sprintf(s, "module_%d", i); 
		vect[i] = new prodcons_sol(s);
	}

	init();
	clear_screen();

	{
		measure_time t;
		sc_start();
	}

	if (auto_wait)
		sleep(1);

	return 0;
}
