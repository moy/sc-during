/********************************************************************
 * Copyright (C) 2014 by Verimag                                    *
 * Initial author: Sancassa Gaëtan                                  *
 * This file must not be distributed outside Verimag                *
 ********************************************************************/

/*!
	The main purpose of this example is to try to make communication between 3 modules using 1 sc_fifo.
	
*/
#include <systemc>
#include "during.h"    	

using namespace std;
using namespace sc_core;
	


class write_if : virtual public sc_interface {
	public:
		virtual void write(char) = 0;
		virtual void reset() = 0;
};

class read_if : virtual public sc_interface {
	public:
		virtual void read(char &) = 0;
		virtual int num_available() = 0;
};

class fifo : public sc_channel, public write_if, public read_if {

	private:
	enum e { max = 10 };
	char data[max];
	int num_elements, first;
	sc_event write_event,
	read_event;

	public:
	fifo(sc_module_name name) : sc_channel(name), num_elements(0), first(0)
	{ }

	void write(char c) {
		if (num_elements == max){
			wait(read_event);
		}
		data[(first + num_elements) % max] = c;
		++ num_elements;
		write_event.notify();
	}

	void read (char &c) {
		if (num_elements == 0) {
			wait(write_event);
		}
		c = data[first];
		-- num_elements;
		first = (first + 1) % max;
		read_event.notify();
	}

	void reset() { num_elements = first = 0; }
	int num_available() { return num_elements;}
};

SC_MODULE(producer) {
	public:
	sc_port<write_if> out; 
		
	SC_CTOR(producer) {
		SC_THREAD(main_action);
	}

	void main_action() {
		const char *str = "Hello, this is a test!\n";
		while (*str){
			wait(3, SC_NS);
			const char temp = *str++;
			out->write(temp);
			L_COUT << "producer: write (" << sc_time_stamp() << "):" << temp << endl;
		}
	} 
};

SC_MODULE(consumer), sc_during {
	public:
	sc_port<read_if> in; 
 
	SC_CTOR(consumer) {
		SC_THREAD(lire);
	}

	void lire() {
		for(;;) {
			during(1, SC_NS, [this] { read_boucle(); });
		}
	}

	
	void read_boucle() {
		char c;
		const char* name = sc_core::sc_get_current_process_b()->get_parent()->basename();
		in->read(c);
		L_COUT << name << " : lire (" << sc_time_stamp() << "):" << c << endl;
	}
}; 

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	fifo f("FIFO");
	f.reset();
	
	consumer c1("cons1");
	c1.in(f);
	consumer c2("cons2");
	c2.in(f);
	producer p("producer");
	p.out(f);
	{
		measure_time t;
		sc_start(20,SC_NS);
	}

	return 0;
}
