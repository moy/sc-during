/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file example-extra-time.cpp
  \brief Example of extra-time, for the paper

  
*/

#include <systemc>
#include "during.h"
#include <iostream>

using namespace std;
using namespace sc_core;


void debug(string s) {
  cout << sc_time_stamp() 
       << ": " << s << endl;
}

SC_MODULE(A), sc_during
{
  void compute() {
    wait(5, SC_MS);
    during(5, SC_MS, [this] { f(); });
    debug("done");
  }
  
  SC_CTOR(A) {
    SC_THREAD(compute);
  }
  
  void f(void) {
    debug("before");
    extra_time(5, SC_MS);
    debug("after");
  };
};

int sc_main(int, char **) {
	A a("a");
	sc_start();
	return 0;
}
