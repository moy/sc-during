/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file timing.cpp
  \brief Test that during actually takes the right time

  
*/
#include <systemc.h>
#include "during.h"
#include "utils/io-lock.h"

extern void P1();
extern void P2();
extern void Q1();
extern void Q2();

void debug(std::string s) {
  L_COUT << sc_time_stamp() 
	 << ": " << s << endl;
}

void P1() {
	debug("P1()");
}
void P2() {
	debug("P2()");
}
void Q1() {
	debug("Q1()");
}
void Q2() {
	debug("Q2()");
}

struct P : sc_module, sc_during {
	void compute() {
		during(25, SC_MS, P1);
		during(12, SC_MS, P2);
	}

	SC_CTOR(P) {SC_THREAD(compute);}
};

struct Q : sc_module, sc_during {
	void compute() {
		wait(10, SC_MS);
		during(13, SC_MS, Q1);
		during(11, SC_MS, Q2);
	}

	SC_CTOR(Q) {SC_THREAD(compute);}
};

int sc_main(int, char **)
{
	P p("p"); Q q("q");
	sc_start(); return 1;
}
