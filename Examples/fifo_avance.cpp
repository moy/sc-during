/********************************************************************
 * Copyright (C) 2014 by Verimag                                    *
 * Initial author: Sancassa Gaëtan                                  *
 * This file must not be distributed outside Verimag                *
 ********************************************************************/

/*!
  The most important aspect in this example is the synchronisation between SystemC and sc_during
	--> spécialement une lecture ne doit pas se faire tant qu'il n'y a pas d'écriture

3 FIFO de communication tq :
A -> B -> C -> A

	Premiere approche :
A: ecrire (3 ns): 100
B: read_boucle (3 ns): Received 100
C: read_boucle (4 ns): Received 100
A: read_boucle (5 ns): Received 100
A: ecrire (8 ns): 101
// TEST-IGNORE: time[] = 14.7603 ms real, 14.421 ms CPU, 11.1773 ms CPU (this thread).

	Seconde approche :
A: ecrire (3 ns): 100
B: read_boucle (4 ns): Received 100
C: read_boucle (5 ns): Received 100
A: read_boucle (6 ns): Received 100
A: ecrire (9 ns): 101
// TEST-IGNORE: time[] = 16.5645 ms real, 14.8792 ms CPU, 2.27196 ms CPU (this thread).

	Troisieme approche :
A: ecrire (3 ns): 100
B: read_boucle (4 ns): Received 100
C: read_boucle (5 ns): Received 100
A: read_boucle (6 ns): Received 100
A: ecrire (9 ns): 101
// TEST-IGNORE: time[] = 21.4536 ms real, 18.503 ms CPU, 11.4883 ms CPU (this thread).




*/

#include <systemc>
#include "during.h"    	

using namespace std;
using namespace sc_core;


void eat_cpu(void) {
	for (int i = 0; i < 100000; ++i)
		for (int j = 0; j < 5540; ++j)
			continue;
}

SC_MODULE(A)
{
	sc_fifo_in<int> input;
	sc_fifo_out<int> output;
	void ecrire() {
		int val = 100;
		for(;;){
			wait(3, SC_NS);
			output.write(val);
			L_COUT << "A: ecrire (" << sc_time_stamp() << "):"<< val << endl;
			val++;
			//eat_cpu();	
			//wait(5, SC_NS);
			read_boucle();
		}

	}
	void read_boucle() {
		int val;

		while (!input.nb_read(val)) {
			//PREMIERE APPROCHE
			//std::cout << "A : Lecture FIFO impossible (" << sc_time_stamp() << ")" <<  std::endl;	
			wait(1, SC_NS);
		}

 		std::cout << "A: read_boucle (" << sc_time_stamp() << "): Received " << val << std::endl;
	}

	SC_CTOR(A) {
		SC_THREAD(ecrire);
   	}
};


SC_MODULE(B), sc_during
{
	sc_fifo_in<int> input;
	sc_fifo_out<int> output;

	void lire() {
		for(;;) {
			during(1, SC_NS, [this] { read_boucle(); });
		}	
	}

	
	void read_boucle() {
		int val;
 		while (!input.nb_read(val)) {
			//PREMIERE APPROCHE
			//std::cout << "B: Lecture FIFO impossible (" << sc_time_stamp() << ")" <<  std::endl;	
			/*sc_call([this]{
					//std::cout << "B: sc_call (" << sc_time_stamp() << ")" <<  std::endl;
					wait(1, SC_NS);
						});*/
 			
			// SECONDE APPROCHE
			/*//std::cout << "B: Lecture FIFO impossible (" << sc_time_stamp() << ")" <<  std::endl;	
			wait(1, SC_NS);*/

			// TROISIEME APPROCHE
			//std::cout << "B: Lecture FIFO impossible (" << sc_time_stamp() << ")" <<  std::endl;	
			extra_time(1, SC_NS);
			catch_up();

	 	}
 		std::cout << "B: read_boucle (" << sc_time_stamp() << "): Received " << val << std::endl;
		output.write(val);	
	};	

	SC_CTOR(B) {
		SC_THREAD(lire);
   	}
};

SC_MODULE(C), sc_during
{
	sc_fifo_in<int> input;
	sc_fifo_out<int> output;

	void lire() {
		for(;;) {
			during(1, SC_NS, [this] { read_boucle(); });
		}	
	}

	
	void read_boucle() {
		int val;
 		while (!input.nb_read(val)) {
			//PREMIERE APPROCHE
			//std::cout << "C: Lecture FIFO impossible (" << sc_time_stamp() << ")" <<  std::endl;	
			/*sc_call([this]{
					//std::cout << "C: sc_call (" << sc_time_stamp() << ")" <<  std::endl;
					wait(1, SC_NS);
						});*/
 			
			// SECONDE APPROCHE
			/*//std::cout << "C: Lecture FIFO impossible (" << sc_time_stamp() << ")" <<  std::endl;	
			wait(1, SC_NS);*/

			// TROISIEME APPROCHE
			//std::cout << "C: Lecture FIFO impossible (" << sc_time_stamp() << ")" <<  std::endl;	
			extra_time(1, SC_NS);
			catch_up();

	 	}
 		std::cout << "C: read_boucle (" << sc_time_stamp() << "): Received " << val << std::endl;
		output.write(val);	
	};	

	SC_CTOR(C) {
		SC_THREAD(lire);
   	}
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	sc_fifo<int> fifoAB("fifoAB",50);
	sc_fifo<int> fifoBC("fifoBC",50);
	sc_fifo<int> fifoCA("fifoCA",50);
  	/* initialize random seed: */
  	srand (time(NULL));

	A a("a");
	B b("b");
	C c("c");
	a.input(fifoCA);
	a.output(fifoAB);
	b.input(fifoAB);
	b.output(fifoBC);
	c.input(fifoBC);
	c.output(fifoCA);
	{
		measure_time t;
		sc_start(200,SC_NS);
	}
	return 0;
}
