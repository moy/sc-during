#include <systemc>
#include "during.h"    	
#include <mutex>	
#include <string>
#include "during_fifo.hpp"


#define WD // Writer sc-during
#define RD // Reader sc-during

//#define WC // Writer systemC
//#define RC // Reader systemC

#define TIME_READ 2
#define TIME_WRITE 8	
//#define EAT_CPU_READ 1
#define EAT_CPU_WRITE 1

using namespace std;
using namespace sc_core;



void eat_cpu(void) {
	for (int i = 0; i < 10000000; ++i)
		for (int j = 0; j < 5540; ++j)
			continue;
}

SC_MODULE(Reader), sc_during{
	fifo_in<int> input;

	void lire() {
		for(;;) {
#ifdef RD
			during(TIME_READ, SC_NS, [this]{
				int val;
				input.read_during(val);
				std::cout << name()<<": read (" << sc_time_stamp() << "): Received " << val << std::endl;
	 		});
#endif
#ifdef RC
			wait(TIME_READ, SC_NS);
			int val;
			/*if(!input.nb_read(val)) {
	 			std::cout << name()<<": FIFO EMPTY (" << sc_time_stamp() << ")" << std::endl;
				input.read(val);
			}*/
			input.read(val);
			std::cout << name() <<": read (" << sc_time_stamp() << "): Received " << val << std::endl;
#endif
#ifdef EAT_CPU_READ
			eat_cpu();
#endif
		}
	}
	SC_CTOR(Reader) {
		SC_THREAD(lire);
   	}
};


SC_MODULE(Writer), sc_during{
	fifo_out<int> output;

	void ecrire() {
		int val = 100;
		for(;;){
#ifdef WD
			wait(5,SC_NS);
			during(TIME_WRITE, SC_NS, [this,&val]{

				output.write_during(val);
				L_COUT << name() <<": write (" << sc_time_stamp() << "):" << val << endl;
				val++;
	 		});
#endif
#ifdef WC

			wait(TIME_WRITE, SC_NS);
			output.write(val);
			L_COUT << name() <<": write (" << sc_time_stamp() << "):" << val << endl;
			val++;

#endif
#ifdef EAT_CPU_WRITE
			eat_cpu();
#endif

		}
	}
	SC_CTOR(Writer) {
		SC_THREAD(ecrire);
   	}
};

int sc_main(int argc, char* argv[])
{
	L_COUT << argc << argv << endl; // juste pour enlever le warning de compil
	fifo<int> f(10);
	Writer a("A");
	
	Reader b("B");
	
	//C c("C");

	a.output(f);
	
	b.input(f);
	
	
	{
		measure_time t;
		sc_start(30,SC_NS);
	}

	return 0;
}
