/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file simple.cpp
  \brief Test that during actually takes the right time

  
*/
#include <systemc>
#include "during.h"
#include "utils/io-lock.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	void compute() {
		wait(10, SC_NS);
		during(10, SC_NS, [this] { f(); });
	}
	
	void f() {
		L_COUT << "A.f(): before sleep" << endl;
		sleep(2);
		L_COUT << "A.f(): after sleep" << endl;
	};

	void compute2() {
		wait(15, SC_NS);
		L_COUT << "A.compute2(): before sleep" << endl;
		sleep(2);
		L_COUT << "A.compute2(): after sleep" << endl;
	}

	SC_CTOR(A) {
		SC_THREAD(compute);
		SC_THREAD(compute2);
	}
};

int sc_main(int, char **)
{
	A a("a");
	sc_start();
	return 0;
}
