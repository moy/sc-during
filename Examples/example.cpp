/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 * This file must not be distributed outside Verimag                *
 ********************************************************************/

/*!
  \file concurrent-catch-up.h
  \brief Test concurrent catch-up, to detect mis-uses of shared variables

*/

#include <systemc>
#include "during.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	void first() {
		L_COUT << "A: first: " << sc_time_stamp() << endl;
		during(10, SC_NS, [this] { f(); });
		L_COUT << "// TEST-EXPECT: A: first after during: 20 ns" << endl;
		L_COUT << "A: first after during: " << sc_time_stamp() << endl;
	}

	void second() {
		wait(5, SC_NS);
		L_COUT << "A: second: " << sc_time_stamp() << endl;
		during(1, SC_NS, [this] { g(); });
		L_COUT << "// TEST-EXPECT: A: second after during: 6 ns" << endl;
		L_COUT << "A: second after during: " << sc_time_stamp() << endl;
	}
	
	SC_CTOR(A) {
		SC_THREAD(first);
		SC_THREAD(second);
	}
	
	void f(void) {
		L_COUT << "// TEST-IGNORE: A: f1: " << sc_time_stamp() << endl;
		extra_time(100, SC_NS);
		//catch_up();
		L_COUT << "// TEST-IGNORE: A: f2: " << sc_time_stamp() << endl;
	};

	void g(void) {
		L_COUT << "// TEST-IGNORE: A: g1: " << sc_time_stamp() << endl;
		catch_up();
		L_COUT << "A: g2: " << sc_time_stamp() << endl;
	}
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	{
		measure_time t;
		sc_start();
	}

	return 0;
}
