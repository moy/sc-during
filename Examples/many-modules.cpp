/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 * This file must not be distributed outside Verimag                *
 ********************************************************************/

/*!
  \file call.cpp
  \brief Test function call from task to SystemC

  
*/

#include <systemc>
#include "during.h"
#include "utils/io-lock.h"

using namespace std;
using namespace sc_core;
// 1s on bauges.imag.fr
	void eat_cpu(void) {
		for (int i = 0; i < 100000; ++i)
			for (int j = 0; j < 5540; ++j)
				continue;
	}
SC_MODULE(A), sc_during
{
	
	void compute_with_sc_during() {
		L_COUT << "A 1: " << sc_time_stamp() << endl;
		wait(10, SC_NS);
		L_COUT << "A 2: " << sc_time_stamp() << endl;
		during(10, SC_NS, [this]{
				L_COUT << "A.f()" << endl;
				eat_cpu();
				sc_call([this]{
						wait(10, SC_NS);
					});
				L_COUT << "A.f() end" << endl;
			});
		L_COUT << "A 3: " << sc_time_stamp() << endl;
	}

	void compute_with_sc_during_on_systemC() {
		L_COUT << "A 1: " << sc_time_stamp() << endl;
		wait(10, SC_NS);
		L_COUT << "A 2: " << sc_time_stamp() << endl;
		during(10, SC_NS, [this]{
				L_COUT << "A.f()" << endl;
				sc_call([this]{
						eat_cpu();
					});
				sc_call([this]{
						wait(10, SC_NS);
					});
				L_COUT << "A.f() end" << endl;
			});
		L_COUT << "A 3: " << sc_time_stamp() << endl;
	}

	void compute_without_sc_during() {
		L_COUT << "A 1: " << sc_time_stamp() << endl;
		wait(10, SC_NS);
		L_COUT << "A 2: " << sc_time_stamp() << endl;
		L_COUT << "A.f()" << endl;
		eat_cpu();
		wait(10, SC_NS);
		wait(10, SC_NS);
		L_COUT << "A.f() end" << endl;
		L_COUT << "A 3: " << sc_time_stamp() << endl;
	}

	SC_CTOR(A) {
		SC_THREAD(compute_with_sc_during);
	}
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A *as[1000];	
	for(int i = 0; i < 1000; i++)
		as[i] = new A("a");
	
	(void)as; // as's elements are used internally by SystemC, but
		  // GCC doesn't notice it.
	L_COUT << "elaboration done, start simulation" << endl;
	{
		measure_time t;
		sc_start();
	}
	L_COUT << "Simulation terminated. Terminating threads" << endl;
	return 0;
}
