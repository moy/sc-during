/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file thread-pool-base.h
  \brief Base class for thread pool implementations

  
*/
#ifndef THREAD_POOL_BASE_H
#define THREAD_POOL_BASE_H

#include <thread>
#include <mutex>
#include <systemc>
#include "sync-task.h"
#include "utils/atomic-variable.h"

/*!

*/
class thread_pool_base {
public:
	static void process_tasks(thread_pool_base *p,
				  sc_core::sc_process_b *sc_process);
	virtual void queue(sync_task *r, sc_core::sc_process_b *current = NULL) = 0;
	int get_nb_threads() const {return m_thread_array.size();}
	thread_pool_base();
	virtual ~thread_pool_base() {/* */}
private:
	virtual sync_task *pop_task(sc_core::sc_process_b *sc_process) = 0;
protected:
	void terminate_simulation() {
		kill_all_threads();
		join_all_threads();
	}

	virtual void kill_all_threads();
	virtual void join_all_threads();
	
	typedef std::thread *thread_ptr;
	std::vector<thread_ptr> m_thread_array;
	thread_ptr add_thread(sc_core::sc_process_b *sc_process = NULL);
	static atomic_variable<int> m_index;
};

#endif // THREAD_POOL_BASE_H
