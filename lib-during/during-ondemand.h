/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file during-ondemand.h
  \brief Implementation of during creating threads on demand

  
*/
#ifndef DURING_ONDEMAND_H
#define DURING_ONDEMAND_H

#include "during-if.h"
#include "sync-task.h"

/**
 * Implementation of during creating threads on demand.
 *
 * By default, no thread is created. On first call to during(), a
 * thread is created, and it is reused by further calls to during().
 */
class sc_during_ondemand : public sc_during_if {
public:
	sc_during_ondemand();

	using sc_during_if::during;
	using sc_during_if::extra_time;
	using sc_during_if::catch_up;

	void extra_time(const sc_core::sc_time & t);

	void catch_up(const sc_core::sc_time & t);

	void sc_call(const std::function<void()> & f);

	void async_sc_call(const std::function<void()> & f);

	void during_done();

	void during(const sc_core::sc_time & time, const std::function<void()> & routine);

	static void terminate_all();

	virtual ~sc_during_ondemand();

	mode get_mode() { return ondemand; }
private:
};

#endif // DURING_ONDEMAND_H
