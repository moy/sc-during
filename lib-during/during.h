/**
 \mainpage SC-DURING Library: Parallel Programming with SystemC
 
 sc-during home page :
 http://www-verimag.imag.fr/~moy/?sc-during-Parallel-Programming-on

 The sc-during library targets loosely timed systems, for which most
 other SystemC parallelization approaches are ineffective. It runs on
 top of any (unmodified) SystemC implementation.

 Installation instructions are available in the INSTALL file of the
 source distribution.

 The testsuite also serves as a set of examples. Start, for example,
 with the file simple.cpp. An example of call to the during library
 is:

 \code
  SC_MODULE(A), sc_during {
          void compute() {
                  // ...
                  during(10, SC_NS, [this] { f(); });
                  // ...
          }
	  // ...
  }
  \endcode

  The lambda function is a way to create a 'void(void)' function
  with bindings to 'this' and other parameters. The std::bind way
  is similar and can also be used.
  This function will be executed during 10
  nanoseconds of simulated time.

  The primitives usable from within a during task are described in the
  ::sc_during_if interface.

  At the end of simulation (i.e. after the call to sc_start() and
  before the destructors of modules are called at the end of
  sc_main()), the sc_during::terminate_all() static function should be
  called, to make sure all during tasks are stopped.

  The build infrastructure generated several binaries for this source
  program:

  - simple-seq is the sequential version, used as reference for
    testing. See during-seq.h.

  - simple-thread uses the thread policy, where one OS thread is
    created for each during task. See during-thread.h.

  - simple-ondemand creates a new OS thread when a during task is
    started, and reused for further invocations. See
    during-ondemand.h.

  - simple-pool pre-allocates a fixed number of threads, and sends
    during tasks to it with a FIFO queue. See during-pool.h.

  - simple-env choses the policy at runtime, according to the
    environment variable $SC_DURING_MODE (whose acceptable values are
    "ondemand", "seq", "pool", "thread"). Defaults to ondemand. See
    during-env.h.

 */

#ifndef __DURING_H__
#define __DURING_H__

#include <systemc>

#include "utils/measure-time.h"

#include "during-if.h"
#include "during-seq.h"
#include "during-thread.h"
#include "during-pool.h"
#include "during-ondemand.h"
#include "during-env.h"

#ifndef DURING_MODE
#define DURING_MODE env
#endif

// Two levels of macros to make sure s is expanded, just like the
// usual str/xstr trick.
#define SC_CLASS_NAME(s) X_SC_CLASS_NAME(s)
#define X_SC_CLASS_NAME(s) sc_during_##s

/**
 * Class to use in user code. Will redirect to the right sc_during_*
 * class.
 */
typedef SC_CLASS_NAME(DURING_MODE) sc_during;

#endif
