/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file during-thread.cpp
  \brief body for the file during-thread.h

  
*/

#include "during-thread.h"
#include "utils/io-lock.h"

using namespace std;

sc_during_thread::sc_during_thread()  {
	// Instanciate before going multithread
	task_manager::get_instance();
	L_COUT << "// TEST-IGNORE: sc_during_thread instanciated" << endl;
	m_current_thread = NULL;
}

void sc_during_thread::extra_time(const sc_core::sc_time & t) {
	task_manager::get_instance()->get_current_task()->extra_time(t);
}

void sc_during_thread::catch_up(const sc_core::sc_time & t) {
	task_manager::get_instance()->get_current_task()->catch_up(t);
}

void sc_during_thread::sc_call(const std::function<void()> & f) {
	task_manager::get_instance()->get_current_task()->sc_call(f);
}

void sc_during_thread::async_sc_call(const std::function<void()> & f) {
	task_manager::get_instance()->get_current_task()->async_sc_call(f);
}

void sc_during_thread::during_done() {
	task_manager::get_instance()->get_current_task()->during_done();
}

void sc_during_thread::during(const sc_core::sc_time & time,
		const std::function<void()> & routine) {
	sync_task r(routine, time);
	D_COUT << "// TEST-IGNORE: [" << &r << ", " << std::this_thread::get_id() << "] start during() thread" << std::endl;
	std::thread t(call_routine, &r);
	m_current_thread = &t;
	m_current_task = &r;
	r.wait_for_completion();
	m_current_thread = NULL;
	t.join();
	D_COUT << "// TEST-IGNORE: end during() thread" << std::endl;
}

/* virtual */ void sc_during_thread::terminate_thread() {
	if (m_current_thread) {
		// the simulation stopped, but our task didn't
		// yet. The t.join() in during() is in the
		// same OS thread as we are, but in a
		// different SystemC SC_THREAD. The end of
		// during() is never going to be executed if
		// we reach this point => do the work
		// ourselves.
		m_current_task->terminate();
		m_current_thread->join();
	}
	m_current_thread = NULL;
}

/* static */ void sc_during_thread::terminate_all() {
	D_COUT << "// TEST-IGNORE: sync_task::terminate_all();" << std::endl;
	sync_task::terminate_all();
	for (auto i : get_instances()) {
		D_COUT << "// TEST-IGNORE: i->terminate_thread();" << std::endl;
		i->terminate_thread();
	}
	D_COUT << "// TEST-IGNORE: done terminate_all();" << std::endl;
}

/* static */ void sc_during_thread::call_routine(sync_task *r) {
	// Unoptimized management of extra_time
	try {
		task_manager::get_instance()->set_current_task(r);
		r->process();
		D_COUT << "// TEST-IGNORE: " << r << " thread terminated by end of function" << std::endl;
		task_manager::get_instance()->set_current_task(NULL);
	} catch (sync_task::simulation_over &e) {
		D_COUT << "// TEST-IGNORE: thread terminated by exception simulation_over" << std::endl;
		/*
		 * The exception was thrown by the during
		 * task. The SystemC simulation has
		 * terminated, but we rely on the fact that
		 * the SystemC thread's stack is still there
		 * to catch the exception.
		 */
	}
}
