/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file thread-pool-ondemand.h
  \brief Thread pool creating consumer thread (and task) as needed

  
*/
#ifndef THREAD_POOL_ONDEMAND_H
#define THREAD_POOL_ONDEMAND_H

#include <systemc>
#include <map>
#include "thread-pool-base.h"
#include "sync-task.h"
#include "utils/io-lock.h"

/*!

*/
class thread_pool_ondemand : public thread_pool_base {
public:
	static thread_pool_ondemand *get_instance();
	static void delete_instance();

	void queue(sync_task *r, sc_core::sc_process_b *current = NULL);

private:
	sync_task *pop_task(sc_core::sc_process_b *current);

	void kill_all_threads();

private:
	static thread_pool_ondemand *m_instance;
	thread_pool_ondemand();
	~thread_pool_ondemand();

	sync_task *m_pending_task;

	struct task_and_consumer {
		sync_task *m_task;
		thread_ptr m_thread;
		bool m_done;
		task_and_consumer() 
			: m_task(NULL), m_thread(NULL), m_done(false) {/* */}
	};
	
	typedef std::map<sc_core::sc_process_b *, task_and_consumer> task_map_t;
	task_map_t m_task_map;

	std::condition_variable m_cond;
	std::mutex m_mut;
};

#endif // THREAD_POOL_ONDEMAND_H
