/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file io-lock.h
  \brief Utility class to temporarily lock stdout or stderr

  
*/
#ifndef IO_LOCK_H
#define IO_LOCK_H

//#define SC_DURING_DEBUG

#include <thread>
#include <mutex>
#include "mutex-wrapper.h"
#include <iostream>

/**
 * Global lock meant to be used as a local variable in a for-loop. See
 * L_COUT and L_CERR for example use.
 */
template<int fd>
class cout_lock {
public:
	cout_lock() : i(0) {m_mut.lock();}
	~cout_lock() {m_mut.unlock();}
	void incr() {i++;}
	bool stop() {return i < 1;}
private:
	int i;
	static mutex_t m_mut;
};

template<int fd>
mutex_t cout_lock<fd>::m_mut;

/**
 * Use for variable scope to hold the mutex for a single line.
 *
 * Usage: L_COUT << "foo" << bar << endl;
 */
#define L_COUT \
	for (cout_lock<1> cout_lock_instance;	\
             cout_lock_instance.stop(); \
             cout_lock_instance.incr()) std::cout

/**
 * Same as L_COUT, but for cerr
 */
#define L_CERR \
	for (cout_lock<2> cout_lock_instance;	\
             cout_lock_instance.stop(); \
             cout_lock_instance.incr()) std::cerr

/**
 * Debug COUT. Define SC_DURING_DEBUG to activate.
 */
#ifdef SC_DURING_DEBUG
#define D_COUT L_COUT
#else
#define D_COUT if (0) L_COUT
#endif

/**
 * Verbose debug output.
 */
#define VD_COUT(n) D_COUT << "// TEST-IGNORE: [" \
	<< n << ", " << std::this_thread::get_id() << "] "

#endif // IO_LOCK_H
