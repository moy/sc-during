/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file atomic-variable.h
  \brief Mutexed variable

  
*/
#ifndef ATOMIC_VARIABLE_H
#define ATOMIC_VARIABLE_H

/**
 * Wrapper for a variable to protect read and write accesses with a
 * mutex.
 */
template<typename T>
class atomic_variable {
public:
	atomic_variable(T t) : v(t) {}
	/** Uninitialized initial value */
	atomic_variable() {}

	T read() {
		std::lock_guard<std::mutex> lock(m_mut);
		return v;
	}

	void write(T t) {
		std::lock_guard<std::mutex> lock(m_mut);
		v = t;
	}
	
	T operator++(int) {
		std::lock_guard<std::mutex> lock(m_mut);
		return v++;
	}
	T operator--(int) {
		std::lock_guard<std::mutex> lock(m_mut);
		return v--;
	}
private:
	T v;
	std::mutex m_mut;
};

#endif // ATOMIC_VARIABLE_H
