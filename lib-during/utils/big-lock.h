/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file big-lock.h
  \brief Global lock implementation

  This is obviously bad practice wrt performance, but can be handy
  especially for testing.
*/
#ifndef BIG_LOCK_H
#define BIG_LOCK_H

#include <thread>
#include <mutex>

/*!
 * RAII-style global lock.
 *
 * Contains one mutex per template instance.
 */
template<int dummy>
struct big_lock {
	big_lock() {
		m_big_lock.lock();
		m_unlocked = false;
	}
	~big_lock() {
		if (!m_unlocked) {
			m_unlocked = true;
			m_big_lock.unlock();
		}
	}
	void unlock() {
		m_unlocked = true;
		m_big_lock.unlock();
	}
	void lock() {
		m_unlocked = false;
		m_big_lock.lock();
	}
private:
	static std::mutex m_big_lock;
	bool m_unlocked;
};

template<int dummy>
std::mutex big_lock<dummy>::m_big_lock;

#endif // BIG_LOCK_H
