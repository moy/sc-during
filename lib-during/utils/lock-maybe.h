/********************************************************************
 * Copyright (C) 2014 by Verimag                                    *
 * Initial author: Swadhin Mangaraj and Matthieu Moy                *
 ********************************************************************/

/*!
  \file lock-maybe.h
  \brief Class that may lock a mutex, or do nothing depending on its
  template parameters.
  
*/
#ifndef LOCK_MAYBE_H
#define LOCK_MAYBE_H

#include <thread>
#include <mutex>

/**
 * Exactly equivalent to std::lock_guard<mutex_type> when the second
 * argument is true, but does nothing if the argument is false.
 */
template<typename mutex_type,
	 bool do_lock>
class lock_guard_maybe {

};

template<typename mutex_type>
class lock_guard_maybe<mutex_type, true> : public std::lock_guard<mutex_type> {
public:
	lock_guard_maybe(mutex_type & m) : std::lock_guard<mutex_type>(m) {/* */}

};


template<typename mutex_type>
class lock_guard_maybe<mutex_type, false> {
public:
	lock_guard_maybe(mutex_type & m) {
		(void)m;
	}
};

#endif // LOCK_MAYBE_H
