/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file cpu-affinity.cpp
  \brief body for the file cpu-affinity.h

  
*/

#include "cpu-affinity.h"
#include <sched.h>

int cpu_bind(const unsigned short cpu)
{
	cpu_set_t mask;
	int ret;

	CPU_ZERO(&mask);
	CPU_SET((int)cpu, &mask);
	ret = sched_setaffinity(0, sizeof mask, &mask);

	return ret;
}
