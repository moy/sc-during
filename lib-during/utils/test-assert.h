/********************************************************************
 * Copyright (C) 2014 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file test-assert.h
  \brief Assertion similar to assert(), but not de-activated
  by -DNDEBUG.
  
  This version of assert() should be used in tests, so that we can run
  tests in release mode.
*/
#ifndef TEST_ASSERT_H
#define TEST_ASSERT_H


#define test_assert(c) do { \
		if (!(c)) {							\
			fprintf(stderr, "%s:%d: %s: assertion failure\n", __FILE__, __LINE__, #c); \
			abort(); \
		} \
	} while(0)


#endif // TEST_ASSERT_H
