/********************************************************************
 * Copyright (C) 2013 by Verimag				    *
 * Initial author: Matthieu Moy					    *
 ********************************************************************/

/*!
  \file mutex-wrapper.h
  \brief Wrapper for the std::mutex class


*/
#ifndef MUTEX_WRAPPER_H
#define MUTEX_WRAPPER_H

#include <thread>
#include <mutex>
#include <iostream>

class mutex_wrapper;

// Uncomment one of the alternatives below to chose between
// instrumented and non-instrumented mutex:
//typedef mutex_wrapper mutex_t;
typedef std::mutex mutex_t;

class mutex_wrapper : private std::mutex {
public:
	typedef std::mutex super;

	void lock() {
		std::cout << "mutex_wrapper::lock()" << std::endl;
		super::lock();
		std::cout << "mutex_wrapper::lock() done" << std::endl;
	}

	void unlock() {
		std::cout << "mutex_wrapper::unlock()" << std::endl;
		super::unlock();
		std::cout << "mutex_wrapper::unlock() done" << std::endl;
	}

	bool try_lock() {
		std::cout << "mutex_wrapper::try_lock()" << std::endl;
		return super::try_lock();
	}
};

#endif // MUTEX_WRAPPER_H
