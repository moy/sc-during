/********************************************************************
 * Copyright (C) 2013 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file randomize.h
  \brief Random sleep statements, for stress testing

  
*/
#ifndef RANDOMIZE_H
#define RANDOMIZE_H

#include <stdlib.h>

//#define SC_DURING_INSERT_SLEEP

#ifdef SC_DURING_INSERT_SLEEP
static inline void random_usleep(int i = 1) {
	static bool seeded = false;
	if (!seeded) {
		srand(time(NULL));
		seeded = true;
	}
	int r = rand();
	if (r % 2) { // do nothing half the time
		usleep(float(r) / RAND_MAX * 10000 * i);
	}
}
#else
static inline void random_usleep(int i = 1) {
	(void)i;
}
#endif


#endif // RANDOMIZE_H
