/********************************************************************
 * Copyright (C) 2013 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file set-instances.h
  \brief Keep a list of all instances of a class

  
*/
#ifndef SET_INSTANCES_H
#define SET_INSTANCES_H

#include <set>

/*!
  Derive from this class to keep a list of all instances of a class.

  \code
  class foo : public set_instances<foo> {
	  // ...
  }
  \endcode

  Warning:

  - This is not thread-safe.

  - The template argument must be the class one derives from.

  - only public inheritance is supported
*/
template <class T>
class set_instances {
public:
	set_instances() {
		m_instances.insert(static_cast<T *>(this));
	}
	~set_instances() {
		m_instances.erase(static_cast<T *>(this));
	}
	static std::set<T *> & get_instances() {
		return m_instances;
	}
private:
	static std::set<T *> m_instances;
};

template <class T> std::set<T *> set_instances<T>::m_instances;

#endif // SET_INSTANCES_H
