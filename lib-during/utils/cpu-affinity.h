/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file cpu-affinity.h
  \brief Manage CPU affinity of threads

  
*/
#ifndef CPU_AFFINITY_H
#define CPU_AFFINITY_H

extern int cpu_bind(const unsigned short cpu);

#endif // CPU_AFFINITY_H
