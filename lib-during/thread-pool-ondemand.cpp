/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file thread-pool-ondemand.cpp
  \brief body for the file thread-pool-ondemand.h

  
*/

#include <iostream>
#include "thread-pool-ondemand.h"

using namespace std;

/* static */ thread_pool_ondemand *thread_pool_ondemand::m_instance = NULL;

/* static */ thread_pool_ondemand *thread_pool_ondemand::get_instance() {
	if (m_instance == NULL) {
		m_instance = new thread_pool_ondemand();
	}
	return m_instance;
}

/* static */ void thread_pool_ondemand::delete_instance() {
	L_COUT << "// TEST-IGNORE: Delete thread pool instance" << endl;
	delete m_instance;
	m_instance = NULL;
}

void thread_pool_ondemand::queue(sync_task *r, sc_core::sc_process_b *current /* = NULL */) {
	D_COUT << "// TEST-IGNORE: Queuing task " << r << std::endl;
	std::lock_guard<std::mutex> lock(m_mut);
	if (current == NULL) {
		current = sc_core::sc_get_current_process_handle();
	}
	assert(current != NULL);
	task_and_consumer &tac = m_task_map[current];
	tac.m_task = r;
	tac.m_done = (r == NULL);
	if (tac.m_thread == NULL)
		tac.m_thread = add_thread(current);
	// Several consumers may be waiting, but not
	// necessarily for this thread => wake them all.
	// TODO: We could optimize by having one condition per item
	// it the m_task_map.
	m_cond.notify_all();
}

sync_task *thread_pool_ondemand::pop_task(sc_core::sc_process_b *current) {
	std::unique_lock<std::mutex> lock(m_mut);
	assert(current != NULL);
	task_and_consumer &tac = m_task_map[current];
	assert(tac.m_thread->get_id() == std::this_thread::get_id());
	while(tac.m_task == NULL
			&& tac.m_done == false) {
		m_cond.wait(lock);
	}
	sync_task *res = tac.m_task;
	tac.m_task = NULL;
	D_COUT << "// TEST-IGNORE: Got a task " << res << std::endl;
	return res;
}

void thread_pool_ondemand::kill_all_threads() {
	std::lock_guard<std::mutex> lock(m_mut);
	sync_task::terminate_all();
	for (auto& kv : m_task_map) {
		kv.second.m_done = true;
	}
	m_cond.notify_all();
}

thread_pool_ondemand::thread_pool_ondemand() {
	assert(m_instance == NULL);
}

thread_pool_ondemand::~thread_pool_ondemand() {
	terminate_simulation();
}
