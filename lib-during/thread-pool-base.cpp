/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file thread-pool-base.cpp
  \brief body for the file thread-pool-base.h

  
*/

#include "thread-pool-base.h"
#include "utils/io-lock.h"
#include "task-manager.h"
#include "utils/cpu-affinity.h"

using namespace std;

atomic_variable<int> thread_pool_base::m_index(0);

void thread_pool_base::process_tasks(thread_pool_base *p,
				     sc_core::sc_process_b *sc_process) {
#ifdef SC_DURING_USE_AFFINITY
	int affinity = m_index++ % std::thread::hardware_concurrency();
	L_COUT << "// TEST-IGNORE: affinity = " << affinity << endl;
	cpu_bind(affinity);
#endif
	try {
		while (true) {
			sync_task *r = p->pop_task(sc_process);
			task_manager::get_instance()->set_current_task(r);
			if (r == NULL) {
				L_COUT << "// TEST-IGNORE: Received NULL task, exiting." << endl;
				return;
			}
			//L_COUT << "// TEST-IGNORE: Processing next task" << std::this_thread::get_id() << endl;
			r->process();
		}
	} catch (sync_task::simulation_over& e) {
		L_COUT << "// TEST-IGNORE: simulation_over exception caught, exiting." << endl;
	}
}

thread_pool_base::thread_ptr
thread_pool_base::add_thread(sc_core::sc_process_b *sc_process) {
	thread_ptr t = new std::thread(process_tasks, this, sc_process);
	m_thread_array.push_back(t);
	//L_COUT << "// TEST-IGNORE: add_thread(" << sc_process << ") = " << t->get_id() << endl;
	return t;
}

void thread_pool_base::kill_all_threads() {
	L_COUT << "// TEST-IGNORE: End of simulation, sending stop tasks" << endl;
	sync_task::terminate_all();
	for (unsigned i = 0; i < m_thread_array.size(); i++)
		queue(NULL);
}

void thread_pool_base::join_all_threads() {
	L_COUT << "// TEST-IGNORE: All threads interrupted, now joining" << endl;
	for (unsigned i = 0; i < m_thread_array.size(); i++) {
		m_thread_array[i]->join();
        delete m_thread_array[i];
    }
    m_thread_array.clear();
    L_COUT << "// TEST-IGNORE: all joins completed" << endl;
}

thread_pool_base::thread_pool_base() {
	// Create the instance before going multithread
	task_manager::get_instance(); 					 
}
