/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file task-manager.h
  \brief Allows thread to keep track of their own "task".

  
*/
#ifndef TASK_MANAGER_H
#define TASK_MANAGER_H

#include <functional>
#include <thread>
#include <mutex>
#include <systemc>
#include "utils/io-lock.h"
#include "sync-task.h"

/*!

*/
class task_manager {
public:
	static task_manager *get_instance();
private:
	static task_manager *m_instance;
	task_manager() {}
	~task_manager();

public:
	sync_task *get_current_task() const {return m_current_task;}

	void set_current_task(sync_task *r) {
		m_current_task = r;
	}
private:
	static thread_local sync_task* m_current_task;
};

#endif // TASK_MANAGER_H
