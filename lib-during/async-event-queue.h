/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file async-event-queue.h
  \brief Class to allow notifying SystemC events from a separate thread

  
*/
#ifndef ASYNC_EVENT_QUEUE_H
#define ASYNC_EVENT_QUEUE_H

#include <systemc>
#include "thread-queue.h"

/*!
 * Class to allow notifying SystemC events from a separate thread.
 * 
 * This is possible only using SystemC 2.3 (using
 * async_request_update).
 */
class async_event_queue : public sc_core::sc_prim_channel {
public:
	/*
	 * Can be called from outside SystemC. Programs event e to be
	 * notified during the next update phase.
	 */
	void async_notify_event(sc_core::sc_event &e) {
		m_event_queue.push(&e);
		async_request_update();
	}

private:
	void update() {
		sc_core::sc_event *e = NULL; // disable warning on g++ 4.1
		while (m_event_queue.try_pop_front(e)) {
			e->notify(sc_core::SC_ZERO_TIME);
		}
	}
	thread_queue<sc_core::sc_event *> m_event_queue;
};

#endif // ASYNC_EVENT_QUEUE_H
