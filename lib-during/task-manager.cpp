/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file task-manager.cpp
  \brief body for the file task-manager.h

  
*/

#include "task-manager.h"

task_manager *task_manager::m_instance = NULL;

thread_local sync_task* task_manager::m_current_task;

task_manager *task_manager::get_instance() {
	if (m_instance == NULL) {
		m_instance = new task_manager();
	}
	return m_instance;
}

