/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file sync-task.cpp
  \brief body for the file sync-task.h

  
*/

#include "sync-task.h"

#ifdef SC_CALL_USES_ASYNC_REQUEST_UPDATE
async_event_queue sync_task::m_async_event_queue;
#endif

sync_task::sync_task(const std::function<void()> & routine,
		const sc_core::sc_time & t /* = sc_core::SC_ZERO_TIME*/)
	: m_routine(routine),
	  m_done(false),
	  m_during_task_done(false),
	  m_remaining_time(t),
	  m_catch_up_time(sc_core::SC_ZERO_TIME) {
}

/* static */ void sync_task::terminate_all() {
	VD_COUT("sync_task") << "terminate_all()" << std::endl;
	for (auto t : get_instances()) {
		t->terminate();
	}
}

void sync_task::wait_for_completion() {
	try {
		sc_core::sc_time remaining_time;
		bool done;
		do {
			done = this->wait_task(remaining_time);
			if (!m_during_task_done)
				consume_remaining_time(remaining_time);
			// if done == true, then it has been true
			// since the exit of wait(), and nothing has
			// been executed that can have interfered with
			// consume_remaining_time().
		} while (!done);
	} catch (simulation_over& e) {
		// OK
	}
}

void sync_task::extra_time(const sc_core::sc_time & t) {
	{
		random_usleep();
		std::lock_guard<std::mutex> lock(m_mutex);
		m_remaining_time += t;
	}
	VD_COUT(this) << "extra_time(" << t << ") => "
		<< m_remaining_time << std::endl;
	m_cond_systemc.notify_one();
}

void sync_task::catch_up(const sc_core::sc_time & t) {
	random_usleep();
	std::unique_lock<std::mutex> lock(m_mutex);
	m_catch_up_time = t;
	// m_catch_up_time is used by wait(...), but we don't
	// need to notify the condition, since the use of
	// m_catch_up_time is just an optimization and is not
	// blocking the process (it is not a "condition").
	// We're protected by m_mutex, so concurrent
	// accesses are OK.
	VD_COUT(this) << "entering catch up (" << t << ")" << std::endl;

	while (!(m_remaining_time <= m_catch_up_time) && !m_simulation_over) {
		VD_COUT(this) << "catch_up waiting for condition" << std::endl;
		m_cond_catch_up.wait(lock);
	}
	end_simulation_maybe();
	m_catch_up_time = sc_core::SC_ZERO_TIME;
	wait_for_sc_call_to_finish(lock);
	VD_COUT(this) << "leaving catch up" << std::endl;
}

void sync_task::async_sc_call(const std::function<void()> & f) {
	std::lock_guard<std::mutex> lock(m_mutex);
	async_sc_call_internal(f);
	VD_COUT(this) << "end async_sc_call" << std::endl;
}

void sync_task::sc_call(const std::function<void()> & f) {
	random_usleep();
	std::unique_lock<std::mutex> lock(m_mutex);
	async_sc_call_internal(f);
	wait_for_sc_call_to_finish(lock);
	VD_COUT(this) << "end sc_call" << std::endl;
}

void sync_task::during_done() {
	VD_COUT(this) << "during done" << std::endl;
	throw during_done_exception();
}

void sync_task::process() {
	try {
		m_routine();
	} catch (during_done_exception& e) {
		end_simulation_maybe();
		m_during_task_done = true;
		m_cond_systemc.notify_one();
		notify_systemc_wait();
		// We just notified SystemC that the task is
		// completed, so we do not need to set m_done
		// and notify it again (see below). We
		// actually have to skip this notification
		// because the sync_task may already have been
		// reallocated to another thread.
		return;
	} catch (simulation_over& e) {
		return; // Don't try to touch the mutex, it
		// may have been deallocated already.
	}

	{
		random_usleep();
		std::lock_guard<std::mutex> lock(m_mutex);
		m_done = true;
		// We cannot release the lock here, otherwise
		// the destructor could be run concurrently
		// with this notify(), causing a crash: if the
		// task runs very fast, it can set m_done
		// before the SystemC thread enters the
		// critical section in wait(), which would
		// therefore exit immediately, and let the
		// caller destroy the task while we call
		// notify_one().
		VD_COUT(this) << "end process => m_cond_systemc.notify_all();" << std::endl;
		m_cond_systemc.notify_one();
		random_usleep();
	}
}

void sync_task::terminate() {
	VD_COUT(this) << "terminating task" << std::endl;
	std::lock_guard<std::mutex> lock(m_mutex);
	m_simulation_over = true;
	while (!m_sc_functions.empty()) {
		m_sc_functions.pop();
	}
	m_cond_catch_up.notify_all();
	m_sc_function_executed.notify_all();
}

bool sync_task::wait_task(sc_core::sc_time &remaining_time) {
	VD_COUT(this) << "wait_task()" << std::endl;
	random_usleep();
	std::unique_lock<std::mutex> lock(m_mutex);
	VD_COUT(this) << "wait_task(): mutex acquired" << std::endl;
	VD_COUT(this) << "!m_done = " << m_done << std::endl;
	VD_COUT(this) << "m_sc_functions.empty() = " << m_sc_functions.empty() << std::endl;
	VD_COUT(this) << "m_remaining_time = " << m_remaining_time << std::endl;
	VD_COUT(this) << "!m_simulation_over = " << m_simulation_over << std::endl;
	while ((!m_done) 
			&& m_remaining_time == sc_core::SC_ZERO_TIME
			&& m_sc_functions.empty()
			&& !m_during_task_done
			&& !m_simulation_over) {
		VD_COUT(this) << "m_cond_systemc.wait() " << std::endl;
		m_cond_systemc.wait(lock);
		VD_COUT(this) << "m_cond_systemc.wait() done " << std::endl;
	}
	VD_COUT(this) << "after condition:" << std::endl;
	VD_COUT(this) << "!m_done = " << m_done << std::endl;
	VD_COUT(this) << "m_sc_functions.empty() = " << m_sc_functions.empty() << std::endl;
	VD_COUT(this) << "m_remaining_time = " << m_remaining_time << std::endl;
	VD_COUT(this) << "!m_simulation_over = " << m_simulation_over << std::endl;
	end_simulation_maybe();
	if (!m_sc_functions.empty()) {
		// If we reach this point, it means the
		// separate thread is waiting for
		// m_sc_function_executed to be notified, and
		// no one else is accessing m_sc_functions.
		// It's safe to unlock the mutex.
		// It's actually needed, because m_sc_functions
		// may call sc_core::wait(), which will
		// trigger a context-switch and may never
		// terminate (e.g. if simulation terminates
		// before wait() returns), hence never unlock
		// the lock. If it is the case, the simulation
		// deadlocks when trying to interrupt threads.
		// We must re-lock before setting m_sc_function
		std::function<void()> f;
		while (!m_sc_functions.empty()) {
			f = m_sc_functions.front();
			lock.unlock();
			f();
			lock.lock();
			m_sc_functions.pop();
		}
		m_sc_function_executed.notify_one();
	}
	// If there's no catchup ongoing, just advance
	// SystemC time till the end of the task. If
	// there's a timed catchup, we can let SystemC
	// time advance only until the date required
	// by the catchup. This way, catchup will
	// terminate faster.
	remaining_time = m_remaining_time - m_catch_up_time;
	return (m_done && m_remaining_time == sc_core::SC_ZERO_TIME)
		|| m_during_task_done;
}

void sync_task::end_simulation_maybe() {
	VD_COUT(this) << "end_simulation_maybe()" << std::endl;
	if (m_simulation_over) {
		VD_COUT(this) << "Simulation over, terminating task."
			<< std::endl;
		throw simulation_over();
	}
}

void sync_task::consume_remaining_time(sc_core::sc_time &remaining_time) {
	// remaining_time is passed as argument to make it a
	// local variable. This is needed to be able to call
	// sc_core::wait outside the critical section.
	VD_COUT(this) << "Remaining time to consume: "
		<< remaining_time << std::endl;
	// At this point, m_remaining_time starts
	// being "optimistic": the actual
	// simulation advances, and
	// m_remaining_time doesn't reduce. It reduces
	// right after in consume_remaining_time().
#ifdef SC_CALL_USES_ASYNC_REQUEST_UPDATE
	sc_core::sc_time before = sc_core::sc_time_stamp();
	sc_core::wait(remaining_time, m_systemc_synchro_ev);
	const sc_core::sc_time &time_waited = sc_core::sc_time_stamp() - before;
#else
	sc_core::wait(remaining_time);
	const sc_core::sc_time &time_waited = remaining_time;
#endif
	VD_COUT(this) << "woke up after sc_core::wait at: " 
		<< sc_core::sc_time_stamp() << std::endl;
	{
		random_usleep();
		std::lock_guard<std::mutex> lock(m_mutex);
		m_remaining_time -= time_waited;
	}
	m_cond_catch_up.notify_one();
}

void sync_task::notify_systemc_wait() {
#ifdef SC_CALL_USES_ASYNC_REQUEST_UPDATE	
	m_async_event_queue.async_notify_event(m_systemc_synchro_ev);
#endif
}

void sync_task::async_sc_call_internal(const std::function<void()> & f) {
	random_usleep();
	end_simulation_maybe();
	m_sc_functions.push(f);
	notify_systemc_wait();
	m_cond_systemc.notify_one();
	VD_COUT(this) << "end async_sc_call_internal" << std::endl;
}

void sync_task::wait_for_sc_call_to_finish(std::unique_lock<std::mutex>& lock) {
	while (!m_sc_functions.empty()) {
		VD_COUT(this) << "sc_call: wait(lock)" << std::endl;
		m_sc_function_executed.wait(lock);
		VD_COUT(this) << "sc_call: wait(lock) done" << std::endl;
	}
}
