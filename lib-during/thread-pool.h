/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file thread-pool.h
  \brief Implementation of thread pool with preallocated thread and a FIFO

  
*/
#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include "thread-queue.h"
#include "during-if.h"
#include "thread-pool-base.h"

class thread_pool : public thread_pool_base {
public:
	static thread_pool *get_instance();
	static thread_pool *get_instance(int nb_threads);
	static void delete_instance();
private:
	static thread_pool *m_instance;
	thread_pool(int thread_pool);
	~thread_pool();

public:
	void queue(sync_task *r, sc_core::sc_process_b *current = NULL) {
		(void)current; // the pool takes tasks regardless of
			       // the process they come from
		m_task_queue.push(r);
	}

	int get_queue_size() {return m_task_queue.size();}
	
private:
	sync_task *pop_task(sc_core::sc_process_b *sc_process) {
		(void)sc_process;
		return m_task_queue.pop_front();
	}

	thread_queue<sync_task *> m_task_queue;
};

#endif // THREAD_POOL_H
