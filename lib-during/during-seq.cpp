/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file during-seq.cpp
  \brief body for the file during-seq.h

  
*/

#include "during-seq.h"
#include "utils/io-lock.h"

sc_during_seq::sc_during_seq() {
	L_COUT << "// TEST-IGNORE: sc_during_seq instanciated" << std::endl;
}

void sc_during_seq::extra_time(const sc_core::sc_time & t) {
	// We're in a sequential implementation, don't bother and use
	// wait directly.
	sc_core::wait(t);
}

void sc_during_seq::catch_up(const sc_core::sc_time & t) {
	(void)t; // Ignored in sequential implementation
	sc_core::sc_process_b *current = sc_core::sc_get_current_process_handle();
	time_map_t::iterator it = 
		m_duration_map.find(current);
	sc_core::sc_time time = it->second;
	it->second = sc_core::SC_ZERO_TIME;
	sc_core::wait(time);
}

void sc_during_seq::sc_call(const std::function<void()> & f) {
	// We're already in a SystemC thread, nothing fancy to do.
	f();
}

void sc_during_seq::async_sc_call(const std::function<void()> & f) {
	// We're already in a SystemC thread, nothing fancy to do.
	f();
}

void sc_during_seq::during_done() {

}

void sc_during_seq::during(const sc_core::sc_time & time,
		const std::function<void()> & routine)
{
	sc_core::sc_process_b *current = sc_core::sc_get_current_process_handle();
	m_duration_map[current] = time;
	routine();
	sc_core::wait(m_duration_map[current]);
}
