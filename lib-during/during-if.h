/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file during-if.h
  \brief Common interface to classes implementing during

  
*/
#ifndef DURING_IF_H
#define DURING_IF_H

#include <systemc>
#include <functional>
#include "utils/set-instances.h"

class sc_during_if {
public:
	sc_during_if() {}

	/**
	 * Convenience helper to call std::bind on methods of this
	 * object. Call 
	 * \code
	 * bind_this(&current_object::method, arg1, arg2)
	 * \endcode
	 * instead of
	 * \code
	 * std::bind(&current_object::method, this, arg1, arg2)
	 * \endcode
	 */
	template<typename T>
	std::function<void()> bind_this(void (T::*f) (void)) {
		return std::bind(f, dynamic_cast<T*>(this));
	}

	/**
	 * Increase duration of current task by t
	 */
	virtual void extra_time(const sc_core::sc_time & t) = 0;

	/**
	 * Convenience wrapper around extra_time(), allowing to use a
	 * time value and a time unit.
	 */
	virtual void extra_time(double time, sc_core::sc_time_unit unit) {
		extra_time(sc_core::sc_time(time, unit));
	}

	/**
	 * Blocks the current task until the SystemC time has reached
	 * the end of the task, minus t.
	 *
	 * If t == 0, this means wait until the SystemC time has
	 * reached the end of this task.
	 */
	virtual void catch_up(const sc_core::sc_time & t) = 0;

	/**
	 * Convenience wrapper around catch_up(), allowing to use a
	 * time value and a time unit.
	 */
	virtual void catch_up(double time, sc_core::sc_time_unit unit) {
		catch_up(sc_core::sc_time(time, unit));
	}
	
	/**
	 * Convenience wrapper around catch_up() with SC_ZERO_TIME
	 * argument. This function blocks the task until the SystemC
	 * time reaches the end of the current during task.
	 */
	virtual void catch_up() {
		catch_up(sc_core::SC_ZERO_TIME);
	}

	/**
	 * Call the function f in the context of SystemC. Unlike the
	 * during task, f is allowed to access shared variables
	 * without locking, to call SystemC primitives like notify()
	 * or wait().
	 */
	virtual void sc_call(const std::function<void()> & f) = 0;

	/**
	 * Call the function f in the context of SystemC. This function
	 * is similar to sc_call, except that after the
	 * function call is initiated, the during task does not wait
	 * for the completion of f rather it continues executing the
	 * other instructions in during task.
	 */
	virtual void async_sc_call(const std::function<void()> & f) = 0;

	/**
	 * Check if the during task is finished which should return 
	 * the control to SystemC.
	 */
	virtual void during_done() = 0;

	/**
	 * Create a "during task" of duration time, executing the
	 * function routine.
	 *
	 * The function routine may be executed as a separate OS
	 * thread, and is not allowed to execute SystemC primitives
	 * like wait or notify().
	 */
	virtual void during(const sc_core::sc_time & time, const std::function<void()> & routine) = 0;

	/**
	 * Convenience wrapper around during(), allowing to use a
	 * time value and a time unit.
	 */
	virtual void during(double time, sc_core::sc_time_unit unit, const std::function<void()> & routine) {
		during(sc_core::sc_time(time, unit), routine);
	}

	virtual ~sc_during_if() {}

	enum mode {seq, thread, pool, ondemand};
	virtual mode get_mode() = 0;
private:
};

#endif // DURING_IF_H
