/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file during-env.h
  \brief Version of the during interface that choses the mode at
  runtime with an environment variable.

  
*/
#ifndef DURING_ENV_H
#define DURING_ENV_H

#include "during-if.h"
#include "during-seq.h"
#include "during-thread.h"
#include "during-pool.h"
#include "during-ondemand.h"
#include <stdlib.h>

class sc_during_env : public sc_during_if {
public:
	/** @{
	 * @name Instanciate and delete actual implementation
	 */
	sc_during_env() {
		const char *c_mode = getenv("SC_DURING_MODE");
		if (c_mode == NULL ||
		    !strcmp(c_mode, ""))
			c_mode = "ondemand";
		std::string mode_str = c_mode;
#define MODE(name) if (mode_str == #name) { \
			m_if = new sc_during_ ## name(); \
			m_mode = name; \
		}
		MODE(seq);
		MODE(pool);
		MODE(thread);
		MODE(ondemand);

		if (!m_if) {
			std::cout << "No such mode \"" << mode_str << "\", sorry" << std::endl;
			abort();
		}
	}

	virtual ~sc_during_env() {
		delete m_if; m_if = NULL;
	}
	virtual mode get_mode() { return m_if->get_mode(); }
	/** @} */

	using sc_during_if::during;
	using sc_during_if::extra_time;
	using sc_during_if::catch_up;

	/** @{
	 * @name Forward calls to actual implementation
	 */
	void extra_time(const sc_core::sc_time & t) { m_if->extra_time(t); }
	void catch_up(const sc_core::sc_time & t)   { m_if->catch_up(t); }
	void sc_call(const std::function<void()> & f) {m_if->sc_call(f); }
	void async_sc_call(const std::function<void()> & f) {m_if->async_sc_call(f); }
	void during_done() {m_if->during_done(); }
	void during(const sc_core::sc_time & time,
		    const std::function<void()> & routine) {
		m_if->during(time, routine);
	}

	static void terminate_all() {
		switch (m_mode) {
#undef MODE
#define MODE(name) case name: \
			sc_during_ ## name::terminate_all(); \
			break;

		MODE(seq);
		MODE(pool);
		MODE(thread);
		MODE(ondemand);
		default:
			abort();
		}
	}
	/* @} */

private:
	/** Actual implementation */
	sc_during_if *m_if;
	static sc_during_if::mode m_mode;
};

#endif // DURING_ENV_H
