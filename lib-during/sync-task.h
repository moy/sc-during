/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
 \file sync-task.h
 \brief Data-structure used to represent a sync_task and let it communicate with SystemC

  
 */
#ifndef SYNC_TASK_H
#define SYNC_TASK_H

#define SC_CALL_USES_ASYNC_REQUEST_UPDATE

#include <systemc>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <set>
#include <functional>
#include <queue>
#include "utils/io-lock.h"
#include "utils/set-instances.h"
#include "utils/randomize.h"

#ifdef SC_CALL_USES_ASYNC_REQUEST_UPDATE
#include "async-event-queue.h"
#endif

class sync_task :
	/*
	 * List of task instances (needed for clean termination).
	 * Since tasks are created/deleted in SystemC context, we need
	 * not use any mutex to keep this structure consistant.
	 */
	public set_instances<sync_task> {
private:
	sync_task(const sync_task &) {}

	/**
	 * Utility function for wait_for_completion(). Wait for the
	 * task to complete, or until extra_time() or sc_call() is
	 * called. Does not touch SystemC's simulation time.
	 *
	 * Return true if the task is finished, and sets
	 * the argument remaining_time to the amount of time SystemC should
	 * advance.
	 *
	 * On first call, m_remaining_time is likely to be non-zero, hence
	 * the function may do nothing.
	 */
	bool wait_task(sc_core::sc_time &remaining_time);

	/**
	 * Throw an exception if the simulation is over, to terminate
	 * the current thread.
	 */
	void end_simulation_maybe();

	/**
	 * Let SystemC time advance until the end of the task (using
	 * sc_core::wait), and reduce m_remaining_time accordingly.
	 */
	void consume_remaining_time(sc_core::sc_time &remaining_time);
	
	/**
	 * Give the SystemC process a chance to execute us ASAP. This
	 * may interrupt the ongoing call to sc_core::wait on the
	 * SystemC side.
	 */
	void notify_systemc_wait();

	/**
	 * Internal function call to be used by both sc_call and 
	 * async_sc_call.
	 */
	void async_sc_call_internal(const std::function<void()> & f);

	void wait_for_sc_call_to_finish(std::unique_lock<std::mutex>& lock);
	
public:
	class simulation_over : public std::exception {};

	/** @{
	 * @name API exposed to the SystemC side
	 */

	/**
	 * \brief Build a task running routine, of duration t.
	 *
	 * NOT thread-safe. Normally, this class should only be
	 * instanciated from SystemC threads. Otherwise, use big_lock
	 * to allow concurrent calls to the constructor.
	 */
	sync_task(const std::function<void()> & routine,
		  const sc_core::sc_time & t = sc_core::SC_ZERO_TIME);

	/**
	 * NOT thread-safe.
	 */
	~sync_task() {}

	static void terminate_all();

	/**
	 * \brief Wait for the task to complete.
	 *
	 * Callable only from an SC_THREAD (normally, called only
	 * indirectly from during_if::during()).
	 *
	 * On return, the SystemC time has advanced by the duration of
	 * the task, and the computation in the task is over.
	 */
	void wait_for_completion();

	/** @} */

	/** @{
	 * @name API Exposed to the During Task
	 */

	/**
	 * \brief Increase the duration of the current task by t.
	 *
	 * The SystemC thread is notified, since it may allow it to
	 * run another sc_core::wait().
	 */
	void extra_time(const sc_core::sc_time & t);

	/**
	 * \brief Catch up with SystemC's time.
	 * 
	 * This causes the task to
	 * block until SystemC's time has advanced up to the end of
	 * the current task, or the end of the task minus t if it is
	 * non-null.
	 */
	void catch_up(const sc_core::sc_time & t);

	/**
	 * \brief Function f is called from the during task, but
	 * unlike sc_call the control is returned to the during
	 * task without waiting for completion of f's execution.
	 */
	void async_sc_call(const std::function<void()> & f);

	/**
	 * \brief Call a function f from During task, in mutual
	 * exclusion with SystemC.
	 *
	 * f is scheduled to be executed next time the task
	 * synchronizes with SystemC. sc_call returns after this
	 * function is executed.
	 */
	void sc_call(const std::function<void()> & f);

	/**
	 * \brief Check if the during task is completed and the
	 * control should return to SystemC.
	 *
	 * It may be possible for the during task to be finished
	 * before the wait() is completed. during_done returns the
	 * control to SystemC immediately after the task is completed.
	 */
	void during_done();

	/** @} */

	/** @{
	 * @name To be called by the thread_pool 
	 */

	/**
	 * Runs the routine, and notify the SystemC thread when it's
	 * done.
	 */
	void process();
	/** @} */

public:
	/** @{
	 * @name Testing purpose
	 */

	/**
	 * Function to be called for testing purpose, outside a SystemC thread.
	 *
	 * It's a simple wrapper around wait()
	 */
	void wait_nosc() {
		sc_core::sc_time time;
		bool done = wait_task(time);
		(void)done; // suppress warning with NDEBUG
		assert(done == true);
		assert(time == sc_core::SC_ZERO_TIME);
	}
	/** @} */

	void terminate();
	
private:
	std::function<void()> m_routine;
	class during_done_exception : public std::exception {};

	/** @{
	    @name Task -> SystemC synchronization
	*/
	bool m_done;
	bool m_during_task_done;
	sc_core::sc_time m_remaining_time;
	/** Condition to wake up the SystemC side */
	std::condition_variable m_cond_systemc;
	/** @} */
	std::mutex m_mutex;

	/** @{
	    @name Function call from Task to SystemC
	*/
	std::condition_variable m_sc_function_executed;
	std::queue<std::function<void()>> m_sc_functions;
#ifdef SC_CALL_USES_ASYNC_REQUEST_UPDATE
	// Instanciating this event is not thread-safe.
	sc_core::sc_event m_systemc_synchro_ev;
	static async_event_queue m_async_event_queue;
#endif
	/** @} */

	/** @{
	    @name SystemC -> Task synchronization
	*/
	std::condition_variable m_cond_catch_up;
	sc_core::sc_time m_catch_up_time;
	bool m_simulation_over = false;
	/* @} */
};

#endif // SYNC_TASK_H
