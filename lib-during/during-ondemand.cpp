/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file during-ondemand.cpp
  \brief body for the file during-ondemand.h

  
*/

#include "during-ondemand.h"
#include "thread-pool-ondemand.h"
#include "task-manager.h"

#include <systemc>
using namespace std;

sc_during_ondemand::sc_during_ondemand() {
	thread_pool_ondemand::get_instance();
	L_COUT << "// TEST-IGNORE: sc_during_ondemand instanciated" << endl;
}

/* static */ void sc_during_ondemand::terminate_all() {
	sync_task::terminate_all();
	thread_pool_ondemand::delete_instance();
}

/* virtual */ sc_during_ondemand::~sc_during_ondemand() {
	thread_pool_ondemand::delete_instance();	
}

void sc_during_ondemand::extra_time(const sc_core::sc_time & t) {
	task_manager::get_instance()->get_current_task()->extra_time(t);
}

void sc_during_ondemand::catch_up(const sc_core::sc_time & t) {
	task_manager::get_instance()->get_current_task()->catch_up(t);
}

void sc_during_ondemand::sc_call(const std::function<void()> & f) {
	task_manager::get_instance()->get_current_task()->sc_call(f);
}

void sc_during_ondemand::async_sc_call(const std::function<void()> & f) {
	task_manager::get_instance()->get_current_task()->async_sc_call(f);
}

void sc_during_ondemand::during_done() {
	task_manager::get_instance()->get_current_task()->during_done();
}

void sc_during_ondemand::during(const sc_core::sc_time & time,
				const std::function<void()> & routine) {
	sync_task r(routine, time);
	thread_pool_ondemand::get_instance()->queue(&r);
	r.wait_for_completion();
}


