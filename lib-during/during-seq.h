/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file during-seq.h
  \brief Sequential implementation of during

  
*/
#ifndef DURING_SEQ_H
#define DURING_SEQ_H

#include "during-if.h"
#include <map>

class sc_during_seq : public sc_during_if {
public:
	sc_during_seq();

	using sc_during_if::during;
	using sc_during_if::extra_time;
	using sc_during_if::catch_up;

	void extra_time(const sc_core::sc_time & t);

	void catch_up(const sc_core::sc_time & t);

	void sc_call(const std::function<void()> & f);

	void async_sc_call(const std::function<void()> & f);

	void during_done();

	void during(const sc_core::sc_time & time,
			const std::function<void()> & routine);

	static void terminate_all() {
		// nothing to do
	}

	virtual ~sc_during_seq() {}

	mode get_mode() { return seq; }
private:
	/**
	 * Duration of the current task.
	 */
	typedef std::map<sc_core::sc_process_b *, sc_core::sc_time> time_map_t;
	time_map_t m_duration_map;
};



#endif // DURING_SEQ_H
