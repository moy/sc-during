/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file during-thread.h
  \brief Naive threaded implementation of during

  
*/
#ifndef DURING_THREAD_H
#define DURING_THREAD_H

#include "during-if.h"
#include <thread>
#include <mutex>
#include "task-manager.h"
#include "async-event-queue.h"

class sc_during_thread : public sc_during_if, public set_instances<sc_during_thread> {
public:
	sc_during_thread();

	using sc_during_if::during;
	using sc_during_if::extra_time;
	using sc_during_if::catch_up;

	void extra_time(const sc_core::sc_time & t);
	
	void catch_up(const sc_core::sc_time & t);

	void sc_call(const std::function<void()> & f);

	void async_sc_call(const std::function<void()> & f);

	void during_done();

	void during(const sc_core::sc_time & time,
		    const std::function<void()> & routine);

	/**
	 * Send a termination signal to the thread, and call
	 * thread::join() on it.
	 */
	virtual void terminate_thread();

	static void terminate_all();

	virtual ~sc_during_thread() {
		terminate_thread();
	}

	mode get_mode() { return thread; }

private:

	static void call_routine(sync_task *r);
	
	// read and written to from the SystemC thread => no need to lock.
	std::thread *m_current_thread;
	sync_task *m_current_task;
};

#endif // DURING_THREAD_H
