/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file thread-queue.h
  \brief Queue allowing several threads to communicate

  
*/
#ifndef THREAD_QUEUE_H
#define THREAD_QUEUE_H

#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <iostream>

/*!
  Unbounded queue to let several threads communicate.

  Reading on an empty queue is blocking.
*/
template<typename T>
class thread_queue {
public:
	/** Insert t in the queue */
	void push(const T &t) {
		std::lock_guard<std::mutex> lock(m_mut);
		m_queue.push(t);
		m_cond.notify_all();
	}

	/** Remove the oldest element in the queue. Blocking if the
	 * queue is empty.
	 */
	void pop() {
		std::unique_lock<std::mutex> lock(m_mut);
		while (m_queue.empty()) {
			m_cond.wait(lock);
		}
		m_queue.pop();
	}

	/** Like pop(), but return the element removed */
	T pop_front() {
		std::unique_lock<std::mutex> lock(m_mut);
		while (m_queue.empty()) {
			m_cond.wait(lock);
		}
		// This has to be a copy, since pop() would invalidate
		// any pointer/reference.
		T result = m_queue.front();
		m_queue.pop();
		return result;
	}
	
	/**
	 * Non-blocking version of pop_front()
	 *
	 * Return true and sets "result" if the queue is not empty.
	 * Return false otherwise.
	 */
	bool try_pop_front(T &result) {
		std::lock_guard<std::mutex> lock(m_mut);
		if (m_queue.empty()) {
			return false;
		}
		result = m_queue.front();
		m_queue.pop();
		return true;
	}

	/** Return the oldest element in the queue without removing it. */
	T& front() {
		std::unique_lock<std::mutex> lock(m_mut);
		while (m_queue.empty()) {
			m_cond.wait(lock);
		}
		return m_queue.front();
	}

	/** Return the newest element in the queue without removing it */
	T& back() {
		std::unique_lock<std::mutex> lock(m_mut);
		while (m_queue.empty()) {
			m_cond.wait(lock);
		}
		return m_queue.back();
	}

	/** Number of elements in the queue */
	int size() {
		std::lock_guard<std::mutex> lock(m_mut);
		return m_queue.size();
	}
	
private:
	std::queue<T> m_queue;

	std::condition_variable m_cond;
	std::mutex m_mut;
};

#endif // THREAD_QUEUE_H
