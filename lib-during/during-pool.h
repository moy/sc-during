/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file during-pool.h
  \brief Implementation of during using a preallocated pool of threads

  
*/
#ifndef DURING_POOL_H
#define DURING_POOL_H

#include "during-if.h"
#include "sync-task.h"

class sc_during_pool : public sc_during_if {
public:
	sc_during_pool();

	using sc_during_if::during;
	using sc_during_if::extra_time;
	using sc_during_if::catch_up;

	void extra_time(const sc_core::sc_time & t);

	void catch_up(const sc_core::sc_time & t);

	void sc_call(const std::function<void()> & f);

	void async_sc_call(const std::function<void()> & f);

	void during_done();

	void during(const sc_core::sc_time &, const std::function<void()> & routine);

	static void terminate_all();

	virtual ~sc_during_pool();

	mode get_mode() { return pool; }
private:
	sync_task *m_current_task = NULL;
};

#endif // DURING_POOL_H
