/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file thread-pool.cpp
  \brief body for the file thread-pool.h

  
*/

#include "thread-pool.h"
#include <stdlib.h>
#include <thread>
#include <mutex>
#include <semaphore.h>
#include "during.h"
#include "utils/io-lock.h"
#include "task-manager.h"

using namespace std;

thread_pool *thread_pool::m_instance = NULL;

thread_pool *thread_pool::get_instance() {
	int nb_threads = -1;
	const char *env = getenv("SC_DURING_NB_THREADS");
	if (env) {
		nb_threads = atoi(env);
	}
	if (nb_threads <= 0) {
		nb_threads = std::thread::hardware_concurrency();
	}
	return thread_pool::get_instance(nb_threads);
}

thread_pool *thread_pool::get_instance(int nb_threads) {
	if (m_instance == NULL) {
		m_instance = new thread_pool(nb_threads);
	}
	return m_instance;
}

void thread_pool::delete_instance() {
	if (m_instance) {
		L_COUT << "// TEST-IGNORE: Delete thread pool instance" << endl;
		delete m_instance;
		m_instance = NULL;
	}
}

thread_pool::thread_pool(int nb_threads) {
	assert(m_instance == NULL);
	L_COUT << "// TEST-IGNORE: Initialize producer consumer with " << nb_threads << " threads" << endl;
	// Consumers instanciated once and for all
	for (int i = 0; i < nb_threads; i++) {
		add_thread();
	}
}

thread_pool::~thread_pool() {
	terminate_simulation();
}
