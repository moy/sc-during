/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file during-pool.cpp
  \brief body for the file during-pool.h

  
*/

#include "during-pool.h"
#include "thread-pool.h"
#include "task-manager.h"
#include "thread-queue.h"
#include "utils/io-lock.h"

#include <systemc>
using namespace std;

sc_during_pool::sc_during_pool() {
	thread_pool::get_instance();
	L_COUT << "// TEST-IGNORE: sc_during_pool instanciated" << std::endl;
}

/* static */ void sc_during_pool::terminate_all() {
	sync_task::terminate_all();
	thread_pool::delete_instance();
}

/* virtual */ sc_during_pool::~sc_during_pool() {
	thread_pool::delete_instance();	
}

void sc_during_pool::extra_time(const sc_core::sc_time & t) {
	task_manager::get_instance()->get_current_task()->extra_time(t);
}

void sc_during_pool::catch_up(const sc_core::sc_time & t) {
	task_manager::get_instance()->get_current_task()->catch_up(t);
}

void sc_during_pool::sc_call(const std::function<void()> & f) {
	task_manager::get_instance()->get_current_task()->sc_call(f);
}

void sc_during_pool::async_sc_call(const std::function<void()> & f) {
	task_manager::get_instance()->get_current_task()->async_sc_call(f);
}

void sc_during_pool::during_done() {
	task_manager::get_instance()->get_current_task()->during_done();
}

void sc_during_pool::during(const sc_core::sc_time & time, 
			    const std::function<void()> & routine) {
	sync_task r(routine, time);
	thread_pool::get_instance()->queue(&r);
	r.wait_for_completion();
}


