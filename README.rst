This file is part of the the sc-during distribution, a library for
parallel programming with SystemC.

The sc-during library targets loosely timed systems, for which most
other SystemC parallelization approaches are ineffective. It runs on
top of any (unmodified) SystemC implementation.

For copying conditions, read COPYING.

Getting started:
----------------

If not done already, follow the instructions in INSTALL.

The documentation is maintained as doxygen comments in the code. To
generated HTML documentation, run::

  make doc

You may also prefer reading the comments in the code directly, in
which case lib-during/during.h is a good starting point.

Testing:
--------

Overview
~~~~~~~~

sc-during is extensively and automatically tested. To run all tests,
run simply:

  make check

To run all tests without recompiling, run "make test" instead.

All tests are expected to pass.

Execution of tests
~~~~~~~~~~~~~~~~~~

Most test cases are SystemC platforms that are ran in several modes
(see AddSCExecTest() in CMakeLists.txt). The script that runs the test
in each mode is test.sh, it can be re-ran manually with e.g.

./test.sh --recompile --loop ./simple

This will recompile the appropriate file if needed, and run the test
in an infinite loop (can catch race-conditions that a single execution
would have missed).

Writting your own tests
~~~~~~~~~~~~~~~~~~~~~~~

To write a new test, you should:

* Write a *.cpp file containing the test, and most likely "git add"
  it.

* Declare the file as a test using e.g. AddSCExecTest(your-test) in
  CMakeLists.txt.

* run "make check"

The output of tests is piped through check_output, in sh-lib.sh. For
example, if the stdout of a test contains "// TEST-EXPECT: foo", then
the next line should be exactly "foo". One can write self-checking
tests with e.g.

  cout << "// TEST-EXPECT: x=42" << endl;
  cout << "x=" << x << endl;

Lines starting with "// TEST-IGNORE:" are ignored when comparing the
output of multiple modes.

Testing interleavings and race-conditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Some bugs may happen only on a very unlikely interleaving. To make the
failure more likely to happen, define SC_DURING_INSERT_SLEEP in
randomize.h. This will insert some usleep statements randomly in the
core of the library, e.g. before acquiring mutexes. Simulations will
become much slower.
