#! /bin/sh

basedir=$(cd "$(dirname $0)"; pwd)
. "$basedir"/sh-lib.sh

usage () {
            cat << EOF
Usage: $(basename $0) [options] PROG

Run PROG, and compares the result with reference file (PROG.res).

Options:
	--help		This help message.
	--recompile	Recompile binaries before launching test
	--debug		Show useful information for debugging.
	--loop		Re-run the test in an infinite loop, to find
			possible non-deterministic failures.
EOF
}

prog=""
recompile=0
debug=0
loop=0
opts=""
modes=""

while test $# -ne 0; do
    case "$1" in
        "--help"|"-h")
            usage
            exit 0
            ;;
	"--recompile")
	    recompile=1
	    ;;
	"--debug")
	    debug=1
	    opts="$opts --debug"
	    ;;
	"--loop")
	    loop=1
	    opts="$opts --loop"
	    ;;
        *)
	    if [ "$prog" != "" ]; then
		echo "ERROR: Program specified more than once or unknown option $1"
		usage
		exit 1
	    fi
            prog="$1"
            ;;
    esac
    shift
done

check_prog_is_set "$prog"

if [ "$recompile" = 1 ]; then
    make -j 3 "$prog" || exit 1
fi

log_checking_prog "$prog"

progress

run_prog "$prog"

progress

check_output test-results/"$prog"

progress

skipped=$(head -n 1 test-results/"$prog" | grep '^// TEST-SKIPPED: ')
if [ "$skipped" != "" ]
then
    printf "%s" " [SKIPPED] "
    log echo "$skipped"
elif [ ! -r "$basedir"/"$prog".res ]; then
    status=2
    printf "%s" " [NO REF] "
    log echo "ERROR: missing reference file $basedir/$prog.res"
    log if_you_like_ref
elif ! diff -u "$basedir"/"$prog".res test-results/"$prog" >> test-results/"$prog".log
then
    printf "%s" " [ERROR DIFF] "
    log echo "ERROR: result for $prog differ from reference"
    log if_you_like_ref
    status=1
fi

ok_or_error
