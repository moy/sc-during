/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file bench-during.cpp
  \brief Measur time for various sc-during features.

  
*/

#define DURING_MODE env
#include <systemc>
#include "during.h"
#include "utils/io-lock.h"
#include "utils/test-assert.h"

using namespace std;
using namespace sc_core;

#define BIL 1000000000
int N = 100;
#define M 100

void eat_cpu() {
	int dummy_counter;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < 42000; j++) {
			dummy_counter++;
			__asm("");
		}
	}
}

SC_MODULE(A), sc_during
{
	void compute() {
		cout << "this = " << this << endl;		
		wait(500, SC_MS);
		cout << "N = " << N << endl;
		for (int i = 0;
		     i < N;
		     i++)
			during(BIL/N, SC_NS, [this] { f(); });
		test_assert(m_counter == N);
		L_COUT << "simple OK: " << sc_core::sc_time_stamp() << endl;
		wait(1, SC_SEC);
		for (int i = 0; i < N; i++) // 2.5 -> 3.5s
			during((BIL/N)/M, SC_NS, [this] { g(); });
		L_COUT << "extra_time OK: " << sc_core::sc_time_stamp() << endl;
		wait(1, SC_SEC);
		during(1, SC_SEC, [this] { h(); }); // 4.5 -> 5.5s
		L_COUT << "eat_cpu OK: " << sc_core::sc_time_stamp() << endl;
		wait(1, SC_SEC);
		during(500, SC_MS, [this] { many_extra_time(); }); // 6.5 -> 7.5s
		L_COUT << "many_extra_time OK: " << sc_core::sc_time_stamp() << endl;
	}
	
	SC_CTOR(A) {
		SC_THREAD(compute);
		m_counter = 0;
	}
	
	void f() {
		m_counter++;
	};
	void g() {
		m_counter++;
		for (int j = 0; j < (M - 1); j++) {
			extra_time((BIL/N)/M, SC_NS);
			m_counter++;
		}
	};
	void many_extra_time() {
		int duration = BIL/(N*M)/2;
		L_COUT << N*M << " extra_time()s" << endl;
		measure_time t(name());
		for (int j = 0; j < N*M; j++) {
			extra_time(duration, SC_NS);
		}
	}
	void h() {
		L_COUT << "start h()" << endl;
		eat_cpu();
		L_COUT << "finish h()" << endl;
	}
	int m_counter;
};

int sc_main(int argc, char *argv[])
{
	if (argc > 1)
		N = atoi(argv[1]);
	cout << "N = " << N << endl;
#ifdef NDEBUG
	cout << "release build" << endl;
#else
	cout << "debug build" << endl;
#endif
	A a("a");
	A b("b"), c("c");
	cout << "sc_main: " << sc_time_stamp() << endl;
	{
		measure_time t("Simple during tasks");
		sc_start(2, SC_SEC);
	}
	cout << "sc_main: " << sc_time_stamp() << endl;
	{
		measure_time t("During tasks with extra-time");
		sc_start(2, SC_SEC);
	}
	cout << "sc_main: " << sc_time_stamp() << endl;
	{
		measure_time t("eat_cpu, sequential");
		eat_cpu();
	}
	cout << "sc_main: " << sc_time_stamp() << endl;
	{
		measure_time t("eat_cpu in during tasks");
		sc_start(2, SC_SEC);
	}
	cout << "sc_main: " << sc_time_stamp() << endl;
	{
		measure_time t("many extra_time() in one task");
		sc_start(2, SC_SEC);
	}
	cout << "sc_main: " << sc_time_stamp() << endl;
	return 0;
}
