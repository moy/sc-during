/********************************************************************
 * Copyright (C) 2014 by Verimag                                    *
 * Initial author: Swadhin K Mangaraj                               *
 ********************************************************************/

/*!
  \file async-call.cpp
  \brief Test asynchronous function call from task to SystemC

  
*/

#include <systemc>
#include "during.h"
#include "utils/io-lock.h"
#include "utils/test-assert.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	bool f_done = false, g_done = false;
	void compute() {
		L_COUT << "A 1: " << sc_time_stamp() << endl;
		wait(10, SC_NS);
		L_COUT << "A 2: " << sc_time_stamp() << endl;
		during(10, SC_NS, [this] { g(); });
		test_assert(f_done);
		test_assert(g_done);
		L_COUT << "A 3: " << sc_time_stamp() << endl;
	}
	
	void g() {
		L_COUT << "A.g()" << endl;
		async_sc_call([this] { f(); });
		L_COUT << "A.g() end" << endl;
		g_done = true;
	}
	
	SC_CTOR(A) {
		SC_THREAD(compute);
	}
	
	void A_wait(sc_time t) {
		sc_module::wait(t);
	}
	
	void f() {
		usleep(10000);
		f_done = true;
	};
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	L_COUT << "elaboration done, start simulation" << endl;
	{
		measure_time t;
		sc_start();
	}
	L_COUT << "Simulation terminated. Terminating threads" << endl;
	return 0;
}
