/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file timing.cpp
  \brief Test that during actually takes the right time

  
*/
#include <systemc>
#include "during.h"
#include "utils/io-lock.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	void compute() {
		L_COUT << name() << ": before task" << endl;
		during(5, SC_MS, [this] { f(); });
		L_COUT << name() << ": after task" << endl;
	}
	
	SC_CTOR(A) {
		SC_THREAD(compute);
	}

	void do_wait() {
		// we'll never reach 1 s.
		L_COUT << "waiting 1 s" << endl;
		wait(1, SC_SEC);
		L_COUT << "Done waiting (should never happen)" << endl;
	}
	
	void f() {
		sc_call([this] { do_wait(); });
	};
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	L_COUT << "elaboration done, start simulation" << endl;
	{
		measure_time t;
		sc_start(30, SC_MS);
	}
	L_COUT << "Simulation terminated. Terminating threads" << endl;
	return 0;
}
