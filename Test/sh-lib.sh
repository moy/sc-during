status=0

all_modes="seq
thread
pool
ondemand"

die () {
    echo "ERROR: $@"
    exit 1
}

log () {
    if [ "$debug" = 1 ]; then
	"$@" | tee -a test-results/"$prog".log
    else
	"$@" >> test-results/"$prog".log
    fi
}

progress () {
    if [ "$debug" = 1 ]; then
	:
    else
	printf "."
    fi
}

check_output () {
    expected_active=0
    while read -r line; do
	case "$line" in
	    "// TEST-EXPECT: "*)
		expected_line=$(printf "%s" "$line" | sed 's@^// TEST-EXPECT: @@')
		expected_active=1
		;;
	    *)
		if [ "$expected_active" = "1" ]; then
		    if [ "$line" != "$expected_line" ]; then
			log echo "Expected: $expected_line"
			log echo "  Actual: $line"
			printf '%s' ' [UNEXPECTED] '
			status=1
		    fi
		    expected_active=0
		fi
		;;
	esac
    done < "$1"
    if [ "$expected_active" = 1 ]; then
	log echo "Expected: $expected_line"
	log echo "at end of file"
	printf '%s' ' [UNFINISHED] '
	status=1
    fi
}

log_checking_prog () {
    mkdir -p test-results/"$(dirname "$prog")"
    rm -f test-results/"$1".log
    (debug=1 log printf "checking %s " "$1")
    progress
    progress
    log date
}

check_prog_is_set () {
    if [ "$1" = "" ]; then
	echo "ERROR: please specify a program on the command-line"
	usage
	exit 1
    fi
}

run_prog () {
(
    if [ "$1" = "--mode" ]; then
	shift
	mode="$1"
	shift
	name="$(echo "$1" | sed 's/-env$//')"-"$mode"
	SC_DURING_MODE="$mode"; export SC_DURING_MODE
	msg="in mode $mode"
    else
	name="$1"
	msg=""
    fi
    log echo Running ./"$1" "$msg"
    if [ ! -x ./"$1" ]; then
	printf "%s" " [ CANTRUN ] "
	exit 2
    fi
    SC_DURING_NB_THREADS=4 \
	SYSTEMC_REGRESSION=1 \
	./"$1" > test-results/"$name".raw 2>&1
    exit_code=$?
    if [ "$debug" = 1 ]; then
	cat test-results/"$name".raw
    fi
    echo "Exit code: $exit_code" >> test-results/"$name".raw
    log echo "Exit code: $exit_code"
    if test $exit_code -gt 129 -a $exit_code -le 192; then
	# segfaults & cie
	printf "%s" " [DIED] "
	exit 2
    fi
    grep -v "^// TEST-IGNORE:" test-results/"$name".raw > test-results/"$name"
    # Remove SystemC's copyright message if present (SystemC 2.3 has
    # SYSTEMC_DISABLE_COPYRIGHT_MESSAGE environment variable, but not
    # SystemC 2.2 ...)
    if head -n 7 test-results/"$name" | grep -q '^SystemC Simulation$'; then
	sed -e '1,/SystemC Simulation/d' test-results/"$name" > test-results/"$name".tmp
	mv test-results/"$name".tmp test-results/"$name"
    fi
) || status=$?
}

ok_or_error () {
    if [ "$status" = 0 ]; then
	echo " OK"
	if [ "$loop" = 1 ]; then
	    set -- $opts
	    if [ "$modes" != "" ]; then		
		set -- --modes "$modes" "$@"
	    fi
	    exec "$0" "$@" "$prog"
	fi
    else
	echo " FAIL"
	cat test-results/"$prog".log
	exit "$status"
    fi
}

if_you_like_ref () {
    echo "If you're happy with test-results/$prog, you may run"
    echo 
    echo "  mv 'test-results/$prog' '$prog.res'"
    echo "  git add '$prog.res'"
    echo 
    echo "and restart the test"
}
