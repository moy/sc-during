/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file test-io-lock.cpp
  \brief Test for io-lock.h

  
*/

#include "utils/io-lock.h"
#include <thread>
#include <mutex>
#include <unistd.h>

template<typename T>
T delay(T v, int t) {
	usleep(t);
	return v;
}

void f() {
	L_COUT << "Start " << delay("middle ", 1000) << delay(42, 1000) << " end." << std::endl;
}

int main() {
	std::thread t1(f);
	std::thread t2(f);
	std::thread t3(f);
	std::thread t4(f);
	t1.join();
	t2.join();
	t3.join();
	t4.join();
}
