/********************************************************************
 * Copyright (C) 2013 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file test-set-instances.cpp
  \brief test for set-instances.h

  
*/

#include "utils/set-instances.h"
#include <iostream>
#include "utils/test-assert.h"

using namespace std;

class foo : public set_instances<foo> {
	
};

class A {
	int x;
};

class B {
	int y;
};

class multiple : A, public set_instances<multiple>, B {
public:
	multiple(const char *name) : m_name(name) {}
	const char *m_name;
};

int main() {
	foo f;
	cout << "// TEST-EXPECT: 1" << endl;
	cout << foo::get_instances().size() << endl;
	{
		foo f2;
		cout << "// TEST-EXPECT: 2" << endl;
		cout << foo::get_instances().size() << endl;
	}
	cout << "// TEST-EXPECT: 1" << endl;
	cout << foo::get_instances().size() << endl;
	
	multiple m("m name");
	test_assert(*(multiple::get_instances().begin()) == &m);
	cout << "// TEST-EXPECT: m(m name)" << endl;
	for (auto i : multiple::get_instances()) {
		cout << "m(" << i->m_name << ")";
	}
	cout << endl;
	cout << "// TEST-EXPECT: 1" << endl;
	cout << multiple::get_instances().size() << endl;
}
