#! /bin/sh

basedir=$(cd "$(dirname $0)"; pwd)
. "$basedir"/sh-lib.sh

usage () {
            cat << EOF
Usage: $(basename $0) [options] PROG

Run PROG using different sc_during modes, and compare the results.

Options:
	--help		This help message.
	--recompile	Recompile binaries before launching test.
	--debug		Show useful information for debugging.
	--loop		Re-run the test in an infinite loop, to find
			possible non-deterministic failures.
	--modes "MODE1 MODE2 MODE3"
			During modes to be tested
	--exclude-mode MODE
			Do not test during mode MODE

	PROG	Program to be tested.
EOF
}

prog=""
status=0
debug=0
recompile=0
loop=0
opts=
modes="$all_modes"

normalize_mode () {
    echo "$1" | tr ' ' '\n' | grep .
}

while test $# -ne 0; do
    case "$1" in
        "--help"|"-h")
            usage
            exit 0
            ;;
	"--debug")
	    debug=1
	    opts="$opts --debug"
	    ;;
	"--recompile")
	    recompile=1
	    ;;
	"--loop")
	    loop=1
	    opts="$opts --loop"
	    ;;
	"--modes")
	    shift
	    modes=$(normalize_mode "$1")
	    ;;
	"--exclude-mode")
	    shift
	    modes=$(echo "$modes" | grep -v "^$1\$")
	    ;;
        *)
	    if [ "$prog" != "" ]; then
		echo "ERROR: Program specified more than once"
		usage
		exit 1
	    fi
            prog="$1"
            ;;
    esac
    shift
done

first_mode=$(echo "$modes" | head -n 1)
other_modes=$(echo "$modes" | tail -n +2)

if [ "$other_modes" = "" ]; then
    die "This script cannot run with only one mode. Use simple-test.sh instead."
fi

check_prog_is_set "$prog"
prog_sq=$(echo "$prog" | sed 's@/@\\/@g')

if [ "$recompile" = 1 ]; then
    make -j 3 "$prog"-env || exit 1
fi

log_checking_prog "$prog"

for type in $modes; do
    progress
    run_prog --mode "$type" "$prog"-env
done

progress
check_output test-results/"$prog"-"$first_mode"

for type in $other_modes; do
    progress
    if ! diff -u test-results/"$prog"-"$first_mode" test-results/"$prog"-"$type" >> test-results/"$prog".log
    then
	printf "%s" " [ERROR DIFF] "
	echo "ERROR: result for $type differ from $first_mode implementation" >> test-results/"$prog".log
	status=1
    fi
done

ok_or_error
