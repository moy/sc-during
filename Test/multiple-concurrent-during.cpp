/********************************************************************
 * Copyright (C) 2014 by Verimag                                    *
 * Initial author: Swadhin K Mangaraj                               *
 * This file must not be distributed outside Verimag                *
 ********************************************************************/

/*!  
  \file async-call-multiple.cpp 
  \brief Test synchronous and asynchronous function calls from 
  multiple concurrent during tasks to SystemC.
  
  
*/

#include <systemc>
#include "during.h"
#include "utils/io-lock.h"
#include "utils/test-assert.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	std::condition_variable m_cond;
	std::mutex m_mutex;
	bool m_tasks_can_run;

	int x = 0, y = 0;
	void compute() {
	   	during(10, SC_NS, [this]{ f(); });
		during(10, SC_NS, [this]{ g(); });
		wait(10, SC_NS);
		cout << "// TEST-EXPECT: End of first during task, x = 7" << endl;
		cout << "End of first during task, x = " << x << endl;
		cout << "// TEST-EXPECT: End of second during task, y = 11" << endl;
		cout << "End of second during task, y = " << y << endl;
	}

	void compute2() {
		during(15, SC_NS, [this]{
				for(int i = 0; i < 6; ++i)
				{
					async_sc_call([this, i]{
							usleep(1000);
							y++;
						});
				}
			});
	}

	void f() {
		m_tasks_can_run = false;
		L_COUT << "A.f()" << endl;
		async_sc_call([this]{
				std::unique_lock<std::mutex> l(m_mutex);
				while (!m_tasks_can_run) {
					m_cond.wait(l);
				}
				cout << "Initial call" << endl;
				x++;
			});
		for(int i = 0; i < 5; ++i)
		{
			async_sc_call([this, i]{
					usleep(1000);
					cout << "With lock, i = " << i << endl;
					x++;
				});
		}
		{
			cout << "// TEST-EXPECT: Initial call" << endl;
			std::unique_lock<std::mutex> l(m_mutex);
			m_tasks_can_run = true;
			m_cond.notify_all();
		}
		sc_call([this]{
				x++;
				cout << "Final call" << endl;
			});
	}

	void g() {
		L_COUT << "A.g()" << endl;
		for(int i = 0; i < 5; ++i)
		{
			async_sc_call([this]{
					usleep(10000);		
					y++;
				});
		}	
	}
	
	SC_CTOR(A) {
		SC_THREAD(compute);
		SC_THREAD(compute2);
	}
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	L_COUT << "elaboration done, start simulation" << endl;
	{
		measure_time t;
		sc_start();
	}
	L_COUT << "Simulation terminated. Terminating threads" << endl;
	return 0;
}
