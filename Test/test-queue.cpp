/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file test-queue.cpp
  \brief Unit test for lib-during/thread-queue.h
  
*/

#include "thread-queue.h"
#include <thread>
#include <mutex>
#include <unistd.h>
#include <iostream>
#include "utils/io-lock.h"
#include "utils/test-assert.h"

using namespace std;

thread_queue<int> b;

void producer(int slowness, int size) {
	int i = 1;
	L_COUT << "// TEST-IGNORE: Start producing" << endl;
	for (; i <= size; i++) {
		// Play with timing to stress multiple cases
		usleep(i * slowness);
		b.push(i);
	}
	b.push(-1);
	for (; i <= 2 * size; i++) {
		b.push(i);
	}
	b.push(-2);
};

void consumer(int slowness) {
	int i = 1;
	L_COUT << "Start consuming" << endl;
	while(b.front() != -1) {
		int f = b.pop_front();
		(void)f; // disable warning with NDEBUG
		test_assert(f == i); ++i;
	}
	L_COUT << "First batch done" << endl;
	b.pop();
	while(b.front() != -2) {
		test_assert(b.front() == i); ++i;
		usleep(b.front() * slowness);
		b.pop();
	}
	b.pop();
};

void run_test(int pslow, int cslow, int size) {
	std::thread p(producer, pslow, size);
	std::thread c(consumer, cslow);
	
	p.join();
	c.join();
}

int main () {
	b.push(42);
	test_assert(b.front() == 42);
	b.push(43);
	test_assert(b.front() == 42);
	test_assert(b.back() == 43);
	b.pop();
	test_assert(b.front() == 43);
	b.pop();

	L_COUT << "Threads, producer slower" << endl;
	run_test(300, 1, 50);

	L_COUT << "Threads, producer much slower" << endl;
	run_test(3000, 1, 3);

	L_COUT << "Threads, consumer slower" << endl;
	run_test(1, 300, 100);

	L_COUT << "Threads, same speed" << endl;
	run_test(1, 1, 100);
}
