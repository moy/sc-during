#! /bin/sh

basedir=$(cd "$(dirname $0)"; pwd)
. "$basedir"/sh-lib.sh

usage () {
            cat << EOF
Usage: $(basename $0) PROG

Test that during-env actually works taking the mode from the environment.
EOF
}

prog="$1"
modes="$all_modes"
status=0

check_prog_is_set "$prog"
log_checking_prog "$prog"

for mode in $modes; do
    progress
    SC_DURING_MODE=$mode run_prog "$prog"-env
    grep -q "^// TEST-IGNORE: sc_during_$mode instanciated\$" test-results/"$prog"-env.raw
done

ok_or_error
