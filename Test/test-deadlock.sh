#! /bin/sh

basedir=$(cd "$(dirname $0)"; pwd)
. "$basedir"/sh-lib.sh

"$basedir"/test.sh ./deadlock "$@" --exclude-mode pool || die "test failed"

printf "%s" 'checking that deadlocks occur at the right times .'
SC_DURING_MODE=pool SC_DURING_NB_THREADS=3 "$basedir"/deadlock-env --timeout 1 2>&1 | grep -q '^Timeout occured!$' || die "deadlock-pool did not timeout with 3 threads"
progress
SC_DURING_MODE=pool SC_DURING_NB_THREADS=4 "$basedir"/deadlock-env --timeout 1 2>&1 | grep -q '^Timeout occured!$' && die "deadlock-pool did timeout with 4 threads"
progress
SC_DURING_MODE=seq "$basedir"/deadlock-env --timeout 1 2>&1 | grep -q '^Timeout occured!$' && die "deadlock-seq did timeout"
progress
SC_DURING_MODE=ondemand "$basedir"/deadlock-env --timeout 1 2>&1 | grep -q '^Timeout occured!$' && die "deadlock-ondemand did timeout"
progress

echo " OK"
