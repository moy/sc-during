#include <systemc.h>
#include "during.h"
#include "utils/test-assert.h"

using namespace std;

struct threads_sol : sc_core::sc_module, sc_during
{

	int x;
	int id;
	void proc_A();
	
	SC_HAS_PROCESS(threads_sol);
	threads_sol(sc_module_name name, int _id) 
		: sc_module(name), id(_id) {
		SC_THREAD(proc_A);
	}
	
	static void *fct_A(void *arg);
};


void threads_sol::proc_A()
{	
	for (int i = 0; i < 10; i++) {
		x = id;
		during(10.0, SC_NS, [this] { fct_A(this); });
		test_assert(x == 2 * id);
	}
}


void *threads_sol::fct_A(void *arg)
{
	threads_sol * self = dynamic_cast<threads_sol *>
		(reinterpret_cast<sc_core::sc_module*>(arg));
	test_assert(self->x == self->id);
	self->x = 2 * self->id;
	return NULL;
}

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;

	vector<threads_sol *> vect(150);
	char s[500];
	for(unsigned int i = 0; i < vect.size(); i++) {
		sprintf(s, "module_%d", i); 
		vect[i] = new threads_sol(s, i);
	}
	
	cout << "Starting simulation ..." << endl;

	{
		measure_time t;
		sc_start();
	}

	return 0;
}
