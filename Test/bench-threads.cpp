/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file bench-threads.cpp
  \brief Benchmark to measure thread performances


*/

#define BOOST_ENABLE_ASSERT_HANDLER 1

#include <thread>
#include <mutex>
#include "utils/measure-time.h"
#include "utils/big-lock.h"
#include "thread-pool.h"
#include "task-manager.h"
#include <systemc>
#include "utils/test-assert.h"

using namespace sc_core;
using namespace std;

const int N = 100000;

void f(void) {
	;
}

// 1 second on bauges.imag.fr
void eat_cpu(void) {
	for (int i = 0; i < 100000; ++i)
		for (int j = 0; j < 5540; ++j)
			continue;
}

namespace boost
{
	void assertion_failed(char const * expr, char const * function, char const * file, long line) {
		(void)expr; (void)function; (void)file; (void)line;
		perror("Assertion failed");
	}
}

void send_requests(int n) {
	for (int i = 0; i < n; i++) {
		big_lock<0> l;
		sync_task t(f);
		l.unlock();
		thread_pool::get_instance()->queue(&t);
		t.wait_nosc();
	}
}

void bench_pool_f(int nb_producers, std::function<void()> fn) {
	std::thread **ta = new std::thread *[nb_producers];

	measure_time t("pool (" + std::to_string(nb_producers) + " producers)");

	for (int i = 0; i < nb_producers; ++i)
		ta[i] = new std::thread(fn);
	for (int i = 0; i < nb_producers; ++i)
		ta[i]->join();
	delete [] ta;
}

void bench_pool(int nb_producers) {
	int n = N / nb_producers;
	bench_pool_f(nb_producers, [n] { send_requests(n); });
}

void bench_thread(int nb_threads) {
	std::thread **ta = new std::thread *[nb_threads];

	measure_time t("threads (" + std::to_string(nb_threads) + " concurrent threads)");

	for (int j = 0; j < N/nb_threads; ++j) {
		for (int i = 0; i < nb_threads; ++i)
			ta[i] = new std::thread(f);
		for (int i = 0; i < nb_threads; ++i)
			ta[i]->join();
	}
	delete [] ta;
}

void bench_pool_size(int size) {
	thread_pool::delete_instance();
	thread_pool::get_instance(size);
	cout << "10 tasks with a pool of "
	     << thread_pool::get_instance()->get_nb_threads() << " threads" << endl;
	bench_pool(10);
}

void bench_pool_eat(int nb_producers) {
	cout << nb_producers << " tasks with a pool of "
	     << thread_pool::get_instance()->get_nb_threads() << " threads" << endl;
	measure_time t("pool eat");
	bench_pool_f(nb_producers, eat_cpu);
}

int sc_main (int argc, char **argv) {
	(void)argc; (void)argv;
	thread_pool::get_instance();
	{
		measure_time t("eat cpu");
		eat_cpu();
	}
	{
		measure_time t("sequential call");
		for (int i = 0; i <= N; i++)
			f();
	}
	bench_pool(1);
	bench_pool(2);
	bench_pool(3);
	bench_pool(10);
	bench_pool(100);

	bench_pool_size(1);
	bench_pool_size(2);
	bench_pool_size(3);
	bench_pool_size(10);

	bench_pool_eat(1);
	bench_pool_eat(2);
	bench_pool_eat(3);
	bench_pool_eat(10);

	bench_thread(1);
	bench_thread(2);
	bench_thread(3);
	bench_thread(10);
	{
		measure_time t("create+join");
		for (int i = 0; i <= N; i++) {
			std::thread t(f);
			t.join();
		}
	}
	{
		std::mutex m;
		measure_time t("mutex");
		for (int i = 0; i <= N; i++) {
			m.lock();
			m.unlock();
		}

	}
	return 0;
}
