/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file bench-during.cpp
  \brief Measur time for various sc-during features.

  
*/

#define DURING_MODE env
#include <systemc>
#include "during.h"
#include "utils/io-lock.h"

using namespace std;
using namespace sc_core;

#define BIL 1000000000
int N = 100;
#define M 100

void eat_cpu() {
	int dummy_counter;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < 42000; j++) {
			dummy_counter++;
			__asm("");
		}
	}
}

SC_MODULE(A), sc_during
{
	void compute() {
		wait(500, SC_MS);
		during(0, SC_MS, [this] { many_extra_time(); });
		L_COUT << "many_extra_time OK: " << sc_core::sc_time_stamp() << endl;
		during(1, SC_SEC, [this] { many_extra_time(); });
		L_COUT << "many_extra_time OK: " << sc_core::sc_time_stamp() << endl;
	}
	
	void slow_down() {
		wait(2, SC_SEC);
		L_COUT << "start sleeping" << endl;
		sleep(2);
		L_COUT << "end sleeping" << endl;
		wait(2, SC_SEC);
	}

	SC_CTOR(A) {
		SC_THREAD(compute);
		SC_THREAD(slow_down);
	}

	void many_extra_time() {
		int duration = BIL/(N*M);
		usleep(1000);
		L_COUT << N*M << " extra_time()s" << endl;
		measure_time t(name());
		for (int j = 0; j < N*M; j++) {
			extra_time(duration, SC_NS);
		}
	}
};

int sc_main(int argc, char *argv[])
{
	if (argc > 1)
		N = atoi(argv[1]);
	cout << "N = " << N << endl;
#ifdef NDEBUG
	cout << "release build" << endl;
#else
	cout << "debug build" << endl;
#endif
	A a("a");
	cout << "sc_main: " << sc_time_stamp() << endl;
	{
		measure_time t("many extra_time() in one task");
		sc_start(2, SC_SEC);
	}
	cout << "sc_main: " << sc_time_stamp() << endl;
	sc_start();
	return 0;
}
