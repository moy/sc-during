/********************************************************************
 * Copyright (C) 2014 by Verimag                                    *
 * Initial author: Swadhin Mangaraj and Matthieu Moy                *
 ********************************************************************/

/*!
  \file test-lock-maybe.cpp
  \brief Test for lock-maybe.h

  
*/

#include "utils/lock-maybe.h"
#include <iostream>
#include <vector>
#include <unistd.h>

class Counter {
private:
	std::mutex mutex;
	int value;

public:
	Counter() : value(0) {}

	void increment(){
	        lock_guard_maybe<std::mutex, true> lock1(mutex);
		int tmp = value;
		usleep(1000);
		value = tmp + 1;
	}

	void decrement(){
	        lock_guard_maybe<std::mutex, false> lock1(mutex);
	        --value;
	}

	void get_value() {
		std::cout << value << std::endl;
	}
};

/* Not a very good test, essentially makes sure lock-maybe.h is compilable */
int main() {
	Counter counter;

	std::vector<std::thread> threads;
	for(int i = 0; i < 5; ++i){
	        threads.push_back(std::thread([&counter](){
					for(int i = 0; i < 100; ++i){
						counter.increment();
					}
				}));
	}

	for(auto& thread : threads){
	        thread.join();
	}

	counter.get_value();

	return 0;
}
