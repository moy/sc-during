/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file test-pool.cpp
  \brief Test for the thread pool system

  
*/

#include "thread-queue.h"
#include "task-manager.h"
#include "thread-pool.h"
#include <thread>
#include <mutex>
#include <unistd.h>
#include <iostream>
#include "utils/io-lock.h"
#include "utils/measure-time.h"
#include "utils/atomic-variable.h"
#include <systemc>
#include "utils/big-lock.h"
#include "utils/test-assert.h"

using namespace std;
using namespace sc_core;

atomic_variable<int> count_f(0);

void f() {
	count_f++;
}
void g() {
	count_f--;
}

void send_requests(int n, std::function<void()> fn) {
	for (int i = 0; i < n; i++) {
		big_lock<0> l;
		sync_task t(fn);
		l.unlock();
		thread_pool::get_instance()->queue(&t);
		t.wait_nosc();
		l.lock(); // lock during call to sync_task's destructor.
	}
}


void test_pool(int nb_producers, int nb_req, std::function<void()> fn) {
	std::thread **ta = new std::thread *[nb_producers];
	measure_time t("pool (" + std::to_string(nb_producers) + " producers)");
	
	for (int i = 0; i < nb_producers; ++i)
		ta[i] = new std::thread(send_requests, nb_req, fn);
	for (int i = 0; i < nb_producers; ++i)
		ta[i]->join();
	delete [] ta;
}


const int N = 10;

int sc_main (int argc, char **argv) {
	(void)argc; (void)argv;
	thread_pool::get_instance();
	L_COUT << "Increase count" << endl;
	for (int i = 1; i <= N; ++i)
		for (int j = 1; j <= N; ++j) {
			count_f.write(0);
			test_pool(i, j, f);
			test_assert(count_f.read() == i * j);
		}
	L_COUT << "Mixed ++ and --" << endl;
	for (int i = 1; i <= N; ++i)
		for (int j = 1; j <= N; ++j) {
			count_f.write(0);
			std::thread t(send_requests, i*j, g);
			test_pool(i, j, f);
			t.join();
			test_assert(count_f.read() == 0);
		}
	L_COUT << "Done." << endl;
	return 0;
}
