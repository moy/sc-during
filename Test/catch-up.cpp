/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file catch-up.cpp
  \brief Test for the catch_up feature

  
*/

#include <systemc>
#include "during.h"
#include "utils/test-assert.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	int x;
	void producer() {
		L_COUT << "A: 1" << endl;
		wait(10, SC_NS);
		usleep(100000);
		L_COUT << "A: before x = 10, " << sc_time_stamp() << endl;
		x = 10;
		wait(10, SC_NS);
		usleep(100000);
		L_COUT << "A: before x = 20, " << sc_time_stamp() << endl;
		x = 20;
		usleep(100000);
		wait(100, SC_NS);
		usleep(100000);
		L_COUT << "A: before x = 300, " << sc_time_stamp() << endl;
		x = 300;
	}

	void consumer() {
		during(11, SC_NS, [this] { f(); });
		test_assert(x == 300);
		L_COUT << "// TEST-EXPECT: consumer: 122 ns" << endl;
		L_COUT << "consumer: " << sc_time_stamp() << endl;
	}
	
	SC_CTOR(A) {
		x = 0;
		SC_THREAD(producer);
		SC_THREAD(consumer);
	}
	
	void f(void) {
		// x is probably still 0 at this point.
		catch_up();
		L_COUT << "f: at t=11, x == " << x << endl;
		test_assert(x == 10);
		L_COUT << "f: assertion OK at t=11" << endl;
		usleep(100000); // we may sleep, but SystemC cannot go
				// after t=11 until we terminate or do
				// extra_time.
		test_assert(x == 10);
		extra_time(8, SC_NS);
		L_COUT << "f: at t=19, x == " << x << endl;
		test_assert(x == 10); // t=19, still not 20 ...
		L_COUT << "f: assertion OK at t=19" << endl;
		extra_time(2, SC_NS);
		catch_up();
		L_COUT << "f: at t=21, x == " << x << endl;
		test_assert(x == 20);
		for (int i = 0; i < 50; i++)
			extra_time(1, SC_NS);
		for (int i = 0; i < 5; i++) {
			usleep(100000);
			for (int j = 0; j < 10; j++)
				extra_time(1, SC_NS);
		}
		extra_time(1, SC_NS);
		catch_up();
		test_assert(x == 300);
	};
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	{
		measure_time t;
		sc_start();
	}

	return 0;
}
