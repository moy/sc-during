/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file test-boost.cpp
  \brief Test that I use correctly boost features.

  
*/

#include <functional>
#include <iostream>

using namespace std;

void f() {
	cout << "f executed" << endl;
}
int main() {
	std::function<void()> fn;

	cout << "not yet created" << endl;
	if (!fn.empty())
		fn();

	fn = f;

	cout << "should be called" << endl;
	if (!fn.empty())
		fn();

	fn = NULL;

	cout << "reinitialized" << endl;
	if (!fn.empty())
		fn();
	return 0;
}
