/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file polling.cpp
  \brief Test extra_time with polling

  
*/

#include <systemc>
#include "during.h"

#include "utils/io-lock.h"
#include "utils/atomic-variable.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	atomic_variable<bool> b;

	void producer() {
		L_COUT << "A 1: " << sc_time_stamp() << endl;
		wait(10, SC_NS);
		b.write(true);
	}
	
	void consumer() {
		L_COUT << "B 1: " << sc_time_stamp() << endl;
		during(1, SC_NS, [this] { poll(); });
		L_COUT << "B 2: polling done" << endl;
		L_COUT << "// TEST-IGNORE: " << sc_time_stamp() << endl;
	}
	SC_CTOR(A) : b(false) {
		SC_THREAD(producer);
		SC_THREAD(consumer);
	}
	
	void poll() {
		while (!b.read())
			extra_time(1, SC_NS);
	};
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	{
		measure_time t;
		sc_start();
	}

	return 0;
}
