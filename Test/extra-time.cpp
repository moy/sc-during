/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file timing.cpp
  \brief Test that during actually takes the right time

  
*/
#include <systemc>
#include "during.h"
#include "utils/io-lock.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	void compute() {
		L_COUT << "A 1: " << sc_time_stamp() << endl;
		wait(10, SC_NS);
		L_COUT << "A 2: " << sc_time_stamp() << endl;
		during(5, SC_NS, [this] { f(); });
		L_COUT << "// TEST-EXPECT: A 3: 20 ns" << endl;
		L_COUT << "A 3: " << sc_time_stamp() << endl;
	}
	
	SC_CTOR(A) {
		SC_THREAD(compute);
	}
	
	void f(void) {
		L_COUT << "A.f()" << endl;
		extra_time(5, SC_NS);
		L_COUT << "A.f() (after extra_time)" << endl;
	};
};

SC_MODULE(B), sc_during
{
	void compute() {
		wait(1, SC_NS);
		L_COUT << "B 1: " << sc_time_stamp() << endl;
		during(4, SC_NS, [this] { f(); });
		L_COUT << "// TEST-EXPECT: B 2: 5 ns" << endl;
		L_COUT << "B 2: " << sc_time_stamp() << endl;
		wait(16, SC_NS);
		L_COUT << "B 3: " << sc_time_stamp() << endl;
	}
	
	SC_CTOR(B) {
		SC_THREAD(compute);
	}
	
	void f(void) {
		L_COUT << "B.f()" << endl;
		for (int i = 0; i < 4; i++) {
			//extra_time(1, SC_NS);
		}
	};
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	B b("b");
	{
		measure_time t;
		sc_start();
	}

	return 0;
}
