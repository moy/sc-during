/********************************************************************
 * Copyright (C) 2014 by Verimag                                    *
 * Initial author: Swadhin K Mangaraj                               *
 * This file must not be distributed outside Verimag                *
 ********************************************************************/

/*!
  \file async-call-multiple-order.cpp
  \brief Test checks that multiple asynchronous function calls in
  sequence from multiple during tasks to SystemC are executed in the
  right order.


*/

#include <systemc>
#include "during.h"
#include "utils/io-lock.h"
#include "utils/test-assert.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	std::condition_variable m_cond;
	std::mutex m_mutex;
	bool m_tasks_can_run;

	void compute() {
		during(10, SC_NS, [this]{ calls(); });
	}

	void calls() {
		m_tasks_can_run = false;
		cout << "Launching async calls" << endl;
		for (int i = 0; i <= 100; ++i) {
			async_sc_call([this, i] {
					std::unique_lock<std::mutex> l(m_mutex);
					while (!m_tasks_can_run) {
						m_cond.wait(l);
					}
					cout << "With lock, i = " << i << endl;
				});
			async_sc_call([this, i] {
					cout << "Without lock, i = " << i << endl;
				});
		}
		cout << "All calls launched" << endl;
		{
			std::unique_lock<std::mutex> l(m_mutex);
			m_tasks_can_run = true;
			m_cond.notify_all();
		}
	}

	SC_CTOR(A) {
		SC_THREAD(compute);
	}
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	L_COUT << "elaboration done, start simulation" << endl;
	{
		measure_time t;
		sc_start();
	}
	L_COUT << "Simulation terminated. Terminating threads" << endl;
	return 0;
}
