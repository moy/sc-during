/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file sc-call-opt.cpp
  \brief Test that sc_call can be executed as soon as possible.

  
*/

#include <systemc>
#include "during.h"
#include "utils/io-lock.h"
#include "utils/atomic-variable.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	void compute() {
		L_COUT << "// TEST-EXPECT: A 1: 0 s" << endl;
		L_COUT << "A 1: " << sc_time_stamp() << endl;
		during(10, SC_NS, [this] { f(); });
		L_COUT << "// TEST-EXPECT: A 2: 16 ns" << endl;
		L_COUT << "A 2: " << sc_time_stamp() << endl;
	}

	void g() {
		L_COUT << "// TEST-EXPECT: A.g(): 0 s" << endl;
		L_COUT << "A.g(): " << sc_time_stamp() << endl;
		g_done.write(true);
	}
	
	void tick() {
		for (int i = 0; i <= 10; i++) {
			while (i == 0 && !g_done.read()) {
				// make sure SystemC instants
				// are long enough for others to execute.
				wait(SC_ZERO_TIME);
			}
			wait(2, SC_NS);
		}
	}

	SC_CTOR(A) {
		SC_THREAD(compute);
		SC_THREAD(tick);
		f_done.write(false);
		g_done.write(false);
	}

	void A_wait(sc_time t) {
		L_COUT << "// TEST-IGNORE: A.A_wait(): " << sc_time_stamp() << endl;
		sc_module::wait(t);
		L_COUT << "// TEST-IGNORE: A.A_wait(): done " << sc_time_stamp() << endl;
	}
	
	void f() {
		L_COUT << "A.f(): " << sc_time_stamp() << endl;
		sc_call([this] { g(); });
		sc_time t(6, SC_NS);
		sc_call([this, t] { A_wait(t); });
		L_COUT << "A.f() end: " << endl; // sc_time_stamp()
						 // may vary here.
		f_done.write(1);
	};
	atomic_variable<bool> g_done, f_done;
};

int sc_main(int, char **)
{
#ifndef SC_CALL_USES_ASYNC_REQUEST_UPDATE
	// Test would deadlock otherwise.
	L_COUT << "Test skipped since SC_CALL_USES_ASYNC_REQUEST_UPDATE is not used" << endl;
	return 0;
#endif
	A a("a");
	L_COUT << "elaboration done, start simulation" << endl;
	{
		measure_time t;
		sc_start();
	}
	L_COUT << "Simulation terminated. Terminating threads" << endl;
	return 0;
}
