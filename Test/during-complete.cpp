/********************************************************************
 * Copyright (C) 2014 by Verimag                                    *
 * Initial author: Swadhin K Mangaraj                               *
 ********************************************************************/

/*!
  \file during-complete.cpp
  \brief Test the immediate return of control to SystemC as soon as
  the during task is completed.

  
*/

#include <systemc>
#include "during.h"
#include "utils/io-lock.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	bool m_during_done = false;

	void compute() {
		L_COUT << "A 1: " << sc_time_stamp() << endl;
		wait(10, SC_NS);
		L_COUT << "A 2: " << sc_time_stamp() << endl;
		during(10, SC_NS, [this]{
				L_COUT << "A.f()" << endl;
				during_done();
				L_COUT << "A.f() after end" << endl;
				abort();
			});
		// At this point, SystemC time cannot be > 15 NS.
		// If we reach this point, then during_done() did
		// shorten the during task.
		m_during_done = true;
		L_COUT << "// TEST-EXPECT: A 3: 15 ns" << endl;
		L_COUT << "A 3: " << sc_time_stamp() << endl;
	}

	void prevent_time_elapse() {
		wait(15, SC_NS);
		while(!m_during_done) {
			wait(SC_ZERO_TIME);
		}
		// At this point, SystemC time == (15, SC_NS)
		wait(1, SC_NS);
		L_COUT << "prevent_time_elapse end: " << sc_time_stamp() << endl;
	}
	
	SC_CTOR(A) {
		SC_THREAD(compute);
		SC_THREAD(prevent_time_elapse);
	}
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	L_COUT << "elaboration done, start simulation" << endl;
	{
		measure_time t;
		sc_start();
	}
	L_COUT << "Simulation terminated. Terminating threads" << endl;
	return 0;
}
