/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file polling-catchup.cpp
  \brief Test extra_time with polling, using catch_up to guarantee fairness.

  
*/

#include <systemc>
#include "during.h"

#include "utils/io-lock.h"

using namespace std;
using namespace sc_core;

template<typename T>
class atomic_variable {
public:
	atomic_variable(T t) : v(t) {}

	T read() {
		std::unique_lock<std::mutex> lock(m_mut);
		return v;
	}

	void write(T t) {
		std::unique_lock<std::mutex> lock(m_mut);
		v = t;
	}
private:
	T v;
	std::mutex m_mut;
};

SC_MODULE(A), sc_during
{
	atomic_variable<bool> b;

	void producer() {
		L_COUT << "A 1: " << sc_time_stamp() << endl;
		wait(100, SC_NS);
		b.write(true);
	}
	
	void consumer() {
		L_COUT << "B 1: " << sc_time_stamp() << endl;
		during(1, SC_NS, [this] { poll(); });
		L_COUT << "B 2: polling done" << endl;
		L_COUT << "// TEST-EXPECT: B 3: 101 ns" << endl;
		L_COUT << "B 3: " << sc_time_stamp() << endl;
	}
	SC_CTOR(A) : b(false) {
		SC_THREAD(producer);
		SC_THREAD(consumer);
	}
	
	void poll() {
		while (!b.read()) {
			extra_time(2, SC_NS);
			catch_up();
		}
	};
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	{
		measure_time t;
		sc_start();
	}

	return 0;
}
