/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file args.cpp
  \brief Test passing arguments through c+11 lambdas
  
*/

#include <systemc>
#include "during.h"
#include "utils/io-lock.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	void compute() {
		for(int i = 0; i <= 10; i++) {
			L_COUT << "A " << i << " (before)" << endl;
			// By-value argument i passed to f.
			L_COUT << "A " << i << " (bound)" << endl;
			L_COUT << "// TEST-EXPECT: A.f(" << i << ")" << endl;
			during(10, SC_NS, [this, i] { f(i); });
			L_COUT << "A " << i << " (after)" << endl;
			L_COUT << "// TEST-EXPECT: A.g()" << endl;
			// Short syntax when g takes no argument.
			during(10, SC_NS, [this] { g(); });
			int z = 0;
			// z is passed by reference. Note that
			// std::ref is needed here.
			during(10, SC_NS, [this, &z] { ref(z); });
			L_COUT << "// TEST-EXPECT: A z = 1" << endl;
			L_COUT << "A z = " << z << endl;
		}
	}
	
	SC_CTOR(A) {
		SC_THREAD(compute);
	}
	
	void f(int i) {
		L_COUT << "A.f(" << i << ")" << endl;
	};

	void ref(int &i) {
		L_COUT << "A.f(" << ++i << ")" << endl;
	};

	void g() {
		L_COUT << name() << ".g()" << endl;
	};
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("A");
	L_COUT << "elaboration done, start simulation" << endl;
	{
		measure_time t;
		sc_start();
	}
	L_COUT << "Simulation terminated. Terminating threads" << endl;
	return 0;
}
