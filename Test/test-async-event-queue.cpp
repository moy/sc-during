/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file test-async-event-queue.cpp
  \brief Test for async_event_queue

  
*/

#include <systemc>

#define DURING_MODE thread
#include "during.h"
#include <thread>
#include <mutex>
#include "utils/io-lock.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	void compute() {
		i = 0;
		during(10, SC_NS, [this] { f(); });
	}
	
	void tick() {
		wait(1, SC_NS);
		for (i = 0; i <= 10;) {
			{
				std::unique_lock<std::mutex> lock(m_mut);
				i++;
			}
			m_cond.notify_all();
			usleep(10000); // let the other time to wake up.
			wait(2, SC_NS);
			L_COUT << "tick: " << sc_time_stamp() << " (i = " << i << ")" << endl;
		}
	}

	void m1() {
		L_COUT << "m1: " << sc_time_stamp() << endl;
	}

	void m2() {
		L_COUT << "m2: " << sc_time_stamp() << endl;
	}

	SC_CTOR(A) {
		SC_THREAD(compute);
		SC_THREAD(tick);
		SC_METHOD(m1); sensitive << e1;
		SC_METHOD(m2); sensitive << e2;
	}

	void wait_i(int expected_i) {
		std::unique_lock<std::mutex> lock(m_mut);
		while (i < expected_i) {
			m_cond.wait(lock);
		}
	}
	
	void f() {
		wait_i(2);
		L_COUT << "// TEST-EXPECT: A.f(): 3 ns" << endl;
		L_COUT << "A.f(): " << sc_time_stamp() << endl;
		q.async_notify_event(e1);
		// double-notification works
		q.async_notify_event(e1);
		q.async_notify_event(e2);
		wait_i(4);
		L_COUT << "// TEST-EXPECT: A.f(): 7 ns" << endl;
		L_COUT << "A.f(): " << sc_time_stamp() << endl;
		L_COUT << "// TEST-EXPECT: m1: 7 ns" << endl;
		q.async_notify_event(e1);
	};

	sc_event e1, e2;
	async_event_queue q;

	std::condition_variable m_cond;
	std::mutex m_mut;
	int i;
};

int sc_main(int, char **)
{
#ifndef SC_CALL_USES_ASYNC_REQUEST_UPDATE
	// Would call SystemC's async_request_update, which does not exist.
	L_COUT << "// TEST-SKIPPED: SC_CALL_USES_ASYNC_REQUEST_UPDATE is not used" << endl;
	return 0;
#endif	
	A a("a");
	L_COUT << "elaboration done, start simulation" << endl;
	{
		measure_time t;
		sc_start();
	}
	L_COUT << "Simulation terminated. Terminating threads" << endl;
	return 0;
}
