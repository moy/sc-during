/********************************************************************
 * Copyright (C) 2014 by Verimag                                    *
 * Initial author: Swadhin K Mangaraj                               *
 ********************************************************************/

/*!  
  \file async-call-multiple.cpp 
  \brief Test multiple asynchronous function calls consecutively from 
  multiple during tasks to SystemC.
  
  
*/

#include <systemc>
#include "during.h"
#include "utils/io-lock.h"
#include "utils/test-assert.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	int x = 0;
	void compute() {
	   	during(10, SC_NS, [this]{ g(); });
		wait(10, SC_NS);
		cout << "End of process, x = " << x << endl;
	}
	
	void compute2() {
		compute();
	}
	
	void g() {
		L_COUT << "A.g()" << endl;
		for(int i = 0; i < 3; ++i)
		{
			async_sc_call([this]{
					usleep(10000);		
					x++;
				});
		}		
	}
	
	SC_CTOR(A) {
		SC_THREAD(compute);
		SC_THREAD(compute2);
	}
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	L_COUT << "elaboration done, start simulation" << endl;
	{
		measure_time t;
		sc_start();
	}
	L_COUT << "Simulation terminated. Terminating threads" << endl;
	return 0;
}
