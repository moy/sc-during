#! /bin/sh

basedir=$(cd "$(dirname $0)"; pwd)
. "$basedir"/sh-lib.sh

SH_DURING_NB_THREAD=2 \
    "$basedir"/test.sh ./nested-during "$@" --exclude-mode ondemand || 
die "test failed"

printf "%s" 'checking that deadlocks occur at the right times .'
SC_DURING_MODE=pool SC_DURING_NB_THREADS=1 "$basedir"/nested-during-env --timeout 1 2>&1 | \
    grep -q '^Timeout occured!$' || die "nested-during-pool did not timeout with 1 threads"
progress
SC_DURING_MODE=pool SC_DURING_NB_THREADS=2 "$basedir"/nested-during-env --timeout 1 2>&1 | \
    grep -q '^Timeout occured!$' && die "nested-during-pool did timeout with 2 threads"
progress
SC_DURING_MODE=seq "$basedir"/nested-during-env --timeout 1 2>&1 | \
    grep -q '^Timeout occured!$' && die "nested-during-seq did timeout"
progress
SC_DURING_MODE=ondemand "$basedir"/nested-during-env --timeout 1 2>&1 | \
    grep -q '^Timeout occured!$' || die "nested-during-ondemand did not timeout"
progress

echo " OK"
