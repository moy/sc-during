/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file timing.cpp
  \brief Test nested during tasks

  
*/
#include <systemc>
#include "during.h"
#include "utils/io-lock.h"
#include <signal.h>

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	void compute() {
		L_COUT << "A 1: " << sc_time_stamp() << endl;
		L_COUT << "// TEST-EXPECT: A.f()" << endl;
		during(10, SC_NS, [this] { f(); });
		L_COUT << "// TEST-EXPECT: A 2: 25 ns" << endl;
		L_COUT << "A 2: " << sc_time_stamp() << endl;
	}
	
	SC_CTOR(A) {
		SC_THREAD(compute);
	}
	
	void f() {
		L_COUT << "A.f()" << endl;
		L_COUT << "// TEST-EXPECT: A.g()" << endl;
		sc_call([this] { call_g(); });
	};
	void call_g() {
		during(15, SC_NS, [this] { g(); });
	}
	
	void g() {
		L_COUT << "A.g()" << endl;
	};
};

void timeout(int sig) {
	(void)sig;
	cout << "Timeout occured!" << endl;
	exit(1);
}

int sc_main(int argc, char *argv[])
{

	if (argc > 2 && !strcmp(argv[1], "--timeout")) {
		int t = atoi(argv[2]);
		cout << "// TEST-IGNORE: Setting up timeout of " << t << " seconds." << endl;
		struct sigaction action;
		action.sa_handler = timeout;
		sigaction(SIGALRM, &action, NULL);
		alarm(t);
	}
	A a("a");
	L_COUT << "elaboration done, start simulation" << endl;
	{
		measure_time t;
		sc_start();
	}
	L_COUT << "Simulation terminated. Terminating threads" << endl;
	return 0;
}
