/********************************************************************
 * Copyright (C) 2014 by Verimag                                    *
 * Initial author: Swadhin K Mangaraj                               *
 * This file must not be distributed outside Verimag                *
 ********************************************************************/

/*!
  \file async-call-multiple-wait.cpp
  \brief Test multiple asynchronous function calls with a wait in
  sequence from multiple during tasks to SystemC.


*/

#include <systemc>
#include "during.h"
#include "utils/io-lock.h"
#include "utils/test-assert.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	void compute() {
		during(10, SC_NS, [this]{
				L_COUT << "A.g()" << endl;
				for(int i = 0; i < 3; ++i)
				{
					async_sc_call([this]{
							wait(10000, SC_NS);
							abort();
						});
				}
			});
	}

	void compute2() {
		compute();
	}

	SC_CTOR(A) {
		SC_THREAD(compute);
		SC_THREAD(compute2);
	}
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	L_COUT << "elaboration done, start simulation" << endl;
	{
		measure_time t;
		sc_start(20, SC_NS);
	}
	L_COUT << "Simulation terminated. Terminating threads" << endl;
	return 0;
}
