/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file timing.cpp
  \brief Test that during actually takes the right time

  In pool implementation, pre-allocating 3 threads (Eat_one_thread,
  running for a long time doing extra_time) and then launching a 4th
  task (B) should deadlock.  
*/
#include <systemc>
#include "during.h"
#include "utils/io-lock.h"

#include <signal.h>
#include <unistd.h>

using namespace std;
using namespace sc_core;

SC_MODULE(Eat_one_thread), sc_during
{
	void compute() {
		during(0, SC_SEC, [this] { forever(); });
	}
	
	SC_CTOR(Eat_one_thread) {
		SC_THREAD(compute);
	}
	
	void forever(void) {
		L_COUT << "Eat_one_thread started one task" << endl;
		extra_time(1, SC_NS);
		for (int i = 1; i <= 10; i++) {
			extra_time(5, SC_SEC);
			usleep(1000);
			catch_up();
		}
	};
};

SC_MODULE(B), sc_during
{
	void compute() {
		wait(2, SC_NS);
		L_COUT << "All Eat_one_thread must have started by now" << endl;
		during(4, SC_NS, [this] { f(); });
		L_COUT << "// TEST-EXPECT: B 2: 10 ns" << endl;
		L_COUT << "B 2: " << sc_time_stamp() << endl;
		wait(14, SC_NS);
		L_COUT << "B 3: " << sc_time_stamp() << endl;
	}
	
	SC_CTOR(B) {
		SC_THREAD(compute);
	}
	
	void f(void) {
		L_COUT << "B.f()" << endl;
		for (int i = 0; i < 4; i++) {
			extra_time(1, SC_NS);
		}
	};
};

void timeout(int sig) {
	(void)sig;
	cout << "Timeout occured!" << endl;
	exit(1);
}

int sc_main(int argc, char *argv[])
{
	if (argc > 2 && !strcmp(argv[1], "--timeout")) {
		int t = atoi(argv[2]);
		cout << "// TEST-IGNORE: Setting up timeout of " << t << " seconds." << endl;
		struct sigaction action;
		action.sa_handler = timeout;
		sigaction(SIGALRM, &action, NULL);
		alarm(t);
	}
	Eat_one_thread one("one"), two("two"), three("three");
	B b("b");
	{
		measure_time t;
		sc_start();
	}

	return 0;
}
