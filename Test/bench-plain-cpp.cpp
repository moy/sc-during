/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file bench-plain-cpp.cpp
  \brief Benchmark for plain C++ code

  It's just an example, to experiment with profiling tools.
*/

#define N 100

void eat_cpu() {
	int dummy_counter;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < 42000; j++) {
			dummy_counter++;
			__asm("");
		}
	}
}

void eat_cpu2() {
	int dummy_counter;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < 42000; j++) {
			dummy_counter++;
			__asm("");
		}
	}
}

void almost_free() {
}

void eat_cpu_wrapper () {
	eat_cpu();
	eat_cpu2();
	eat_cpu();
	for (int i = 0; i < N; i++)
		almost_free();
}

void eat_cpu_heavy_wrapper () {
	eat_cpu();
	eat_cpu2();
	int dummy_counter;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < 42000; j++) {
			dummy_counter++;
			__asm("");
		}
	}
}

int main() {
	eat_cpu();
	eat_cpu_wrapper();
	eat_cpu_heavy_wrapper();
}



