/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file call.cpp
  \brief Test function call from task to SystemC

  
*/

#include <systemc>
#include "during.h"
#include "utils/io-lock.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	void compute() {
		L_COUT << "A 1: " << sc_time_stamp() << endl;
		wait(10, SC_NS);
		L_COUT << "A 2: " << sc_time_stamp() << endl;
		during(10, SC_NS, [this]{
				L_COUT << "A.f()" << endl;
				sc_call([this]{
						L_COUT << "A.g()" << endl;
					});
				sc_call([this]{
						wait(10, SC_NS);
					});
				L_COUT << "A.f() end" << endl;
			});
		L_COUT << "A 3: " << sc_time_stamp() << endl;
	}

	SC_CTOR(A) {
		SC_THREAD(compute);
	}
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	L_COUT << "elaboration done, start simulation" << endl;
	{
		measure_time t;
		sc_start();
	}
	L_COUT << "Simulation terminated. Terminating threads" << endl;
	return 0;
}
