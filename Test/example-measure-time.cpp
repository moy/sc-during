/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file example-measure-time.cpp
  \brief Test for measure-time.h utility

  
*/

#include "utils/measure-time.h"
#include <unistd.h>

int main() {
	{
		measure_time t("nothing");
	}
	{
		int i, j;
		measure_time t("real computation with a loop");
		
		/* Real computation (but don't use -O3 ...) */
		for(i = 0; i <= 10000; i++)
			for (j = 0; j <= 100000; j++)
				;
	}

	{
		measure_time t("no real computation, but sleep(1)");
		sleep(1); /* takes wall-clock time, but not CPU time */
	}
	return 0;
}
 
	
