/********************************************************************
 * Copyright (C) 2014 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file async-call-order.cpp
  \brief Test asynchronous function call from task to SystemC, with
  constraints on thread ordering.

  
*/

#include <systemc>
#include "during.h"
#include "utils/io-lock.h"
#include "utils/test-assert.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	std::mutex m_mutex;
	std::condition_variable m_cond;
	bool f_done = false, g_done = false;
	void compute() {
		L_COUT << "A 1: " << sc_time_stamp() << endl;
		wait(10, SC_NS);
		L_COUT << "A 2: " << sc_time_stamp() << endl;
		during(10, SC_NS, [this]{ g(); });
		test_assert(f_done);
		test_assert(g_done);
		L_COUT << "A 3: " << sc_time_stamp() << endl;
	}
	
	void g() {
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			L_COUT << "A.g()" << endl;
			L_COUT << "// TEST-EXPECT: A.g() end" << endl;
			// async_sc_call should be asynchonous, hence
			// f _can_ be executed later that "A.g() end".
			// because of the mutex, it can _only_ be
			// executed after, hence the A.f() output can
			// only follow the A.g() end.
			async_sc_call([this]{ f(); });
			L_COUT << "A.g() end" << endl;
			L_COUT << "// TEST-EXPECT: A.f()" << endl;
			g_done = true;
			while(!f_done) {
				m_cond.wait(lock);
			}
		}
		L_COUT << "A: first section done" << endl;
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			async_sc_call([this]{
					std::unique_lock<std::mutex> lock(m_mutex);
					usleep(10000);
					L_COUT << "A: inside second section" << endl;
				});
			L_COUT << "A: second section done" << endl;
			L_COUT << "// TEST-EXPECT: A: inside second section" << endl;
		}
		catch_up();
		L_COUT << "A: catch_up done" << endl;
	}
	
	SC_CTOR(A) {
		SC_THREAD(compute);
	}
	
	void f() {
		std::unique_lock<std::mutex> lock(m_mutex);
		L_COUT << "A.f()" << endl;
		f_done = true;
		m_cond.notify_one();
		random_usleep();
	};
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	L_COUT << "elaboration done, start simulation" << endl;
	{
		measure_time t;
		sc_start();
	}
	L_COUT << "Simulation terminated. Terminating threads" << endl;
	return 0;
}
