elaboration done, start simulation
m1: 0 s
m2: 0 s
tick: 3 ns (i = 1)
// TEST-EXPECT: A.f(): 3 ns
A.f(): 3 ns
m2: 3 ns
m1: 3 ns
tick: 5 ns (i = 2)
tick: 7 ns (i = 3)
// TEST-EXPECT: A.f(): 7 ns
A.f(): 7 ns
// TEST-EXPECT: m1: 7 ns
m1: 7 ns
tick: 9 ns (i = 4)
tick: 11 ns (i = 5)
tick: 13 ns (i = 6)
tick: 15 ns (i = 7)
tick: 17 ns (i = 8)
tick: 19 ns (i = 9)
tick: 21 ns (i = 10)
tick: 23 ns (i = 11)
Simulation terminated. Terminating threads
Exit code: 0
