/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file timing.cpp
  \brief Test that during actually takes the right time

  
*/
#include <systemc>
#include "during.h"
#include "utils/io-lock.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{
	void compute() {
		L_COUT << name() << ": before task" << endl;
		during(5, SC_MS, [this] { f(); });
		L_COUT << name() << ": after task" << endl;
	}
	
	SC_CTOR(A) {
		SC_THREAD(compute);
	}
	
	void f() {
		while(true) {
			extra_time(10, SC_MS);
			catch_up();
			// At this point, SystemC time = 5 + 10 N
			// => SystemC timing is deterministic.
			// We have a non-deterministic interleaving of
			// a and b, but the output doesn't contain
			// name(), so it's deterministic.
			L_COUT  << " extra_time(1, SC_MS)" << endl;
		}
	};
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	A b("b");
	L_COUT << "elaboration done, start simulation" << endl;
	{
		measure_time t;
		sc_start(30, SC_MS);
	}
	L_COUT << "Simulation terminated. Terminating threads" << endl;
	sc_during::terminate_all();
	return 0;
}
