/********************************************************************
 * Copyright (C) 2011 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file usleep.cpp
  \brief Test playing on wall-clock time with usleep

  
*/

#include <systemc>
#include "during.h"
#include <unistd.h>
#include "utils/io-lock.h"

using namespace std;
using namespace sc_core;

SC_MODULE(A), sc_during
{

	int x;
	void compute() {
		L_COUT << "A 1: " << sc_time_stamp() << endl;
		wait(10, SC_NS);
		L_COUT << "A 2: " << sc_time_stamp() << endl;
		during(10, SC_NS, f);
		L_COUT << "A 3: " << sc_time_stamp() << endl;
	}
	
	SC_CTOR(A) {
		SC_THREAD(compute);
	}
	
	static void f(void) {
		L_COUT << "A.f()" << endl;
	};
};

SC_MODULE(B), sc_during
{

	int x;
	void compute() {
		wait(1, SC_NS);
		L_COUT << "B 1: " << sc_time_stamp() << endl;
		during(8, SC_NS, f);
		L_COUT << "B 2: " << sc_time_stamp() << endl;
		// SystemC is slow, but of course, A should not start
		// right away.
		usleep(400000);
		L_COUT << "B 3: " << sc_time_stamp() << endl;
		wait(14, SC_NS);
		L_COUT << "B 4: " << sc_time_stamp() << endl;
	}
	
	SC_CTOR(B) {
		SC_THREAD(compute);
	}
	
	static void f(void) {
		L_COUT << "B.f() start" << endl;
		// Task is slow, but SystemC has to wait (i.e. A
		// should not execute right now, and B 2 should not
		// occur before B.f() end)
		usleep(400000);
		L_COUT << "B.f() end" << endl;
	};
};

int sc_main(int argc, char *argv[])
{
	(void)argc; (void)argv;
	A a("a");
	B b("b");
	{
		measure_time t;
		sc_start();
	}

	return 0;
}
