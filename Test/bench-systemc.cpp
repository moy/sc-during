/********************************************************************
 * Copyright (C) 2012 by Verimag                                    *
 * Initial author: Matthieu Moy                                     *
 ********************************************************************/

/*!
  \file bench-during.cpp
  \brief Measur time for various sc-during features.

  
*/

#include <systemc>
#include "utils/measure-time.h"
#include <unistd.h>

using namespace std;
using namespace sc_core;

#define BIL 1000000000
int N = 10000;
#define M 100

void eat_cpu() {
	int dummy_counter;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < 42000; j++) {
			dummy_counter++;
			__asm("");
		}
	}
}

SC_MODULE(A)
{
	void compute() {
		wait(2, SC_SEC);
		cout << name() << ": done first wait @" << sc_time_stamp() << endl;
		wait(1, SC_SEC);
		{
		measure_time t("wait (time)");

		for (int i = 0; i < 500000; i++) {
			wait(2, SC_US);
			// cout << name() << ": wait(2, US)" << endl;
		}
		}
		wait(1, SC_SEC);
		cout << name() << ": done 500000 waits @" << sc_time_stamp() << endl;
		wait(1, SC_SEC);
		for (int i = 0; i < 500000; i++)
			wait(SC_ZERO_TIME);
		cout << name() << ": done 500000 waits(0) @" << sc_time_stamp() << endl;
		wait(1, SC_SEC);
	}
	
	SC_CTOR(A) {
		SC_THREAD(compute);
		m_counter = 0;
	}
	
	void f() {
		m_counter++;
	};
	void h() {
		cout << "start h()" << endl;
		eat_cpu();
		cout << "finish h()" << endl;
	}
	int m_counter;
};

int sc_main(int argc, char *argv[])
{
	A a1("a1"), a2("a2");
	if (argc > 1)
		N = atoi(argv[1]);
	cout << "N = " << N << endl;
#ifdef NDEBUG
	cout << "release build" << endl;
#else
	cout << "debug build" << endl;
#endif
	{
		measure_time t("nothing");
	}
	{
		measure_time t("first sc_start()");
		sc_start(250, SC_MS);
	}
	{
		measure_time t("second sc_start()");
		sc_start(250, SC_MS);
	}
	{
		measure_time t("1000 sc_start() (nothing in simulation)");
		for (int i = 0; i < 1000; i++)
			sc_start(1, SC_MS);
	}
	cout << "main(): t1 = " << sc_time_stamp() << endl; // t = 1.5s
	sc_start(1, SC_SEC);
	{
		measure_time t("1,000,000 waits(time)");
		sc_start(2, SC_SEC); // 2.5 -> 4.5
	}
	cout << "main(): t2 = " << sc_time_stamp() << endl; // t = 4.5s
	sc_start(1, SC_SEC);
	{
		measure_time t("1,000,000 waits(0)");
		sc_start(1, SC_SEC); // 5.5 -> 6.5
	}
	cout << "main(): t3 = " << sc_time_stamp() << endl; // t = 6.5s

	return 0;
}
