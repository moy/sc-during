#! /bin/sh

usage () {
            cat << EOF
Usage: $(basename $0) [options]

Options:
	--help		This help message.
	--dir DIR	Install boost in directory DIR
EOF
}

dir=.

while test $# -ne 0; do
    case "$1" in
        "--help"|"-h")
            usage
            exit 0
            ;;
        "--dir")
	    shift
	    dir="$1"
            ;;
        *)
            echo "unrecognized option $1"
            usage
            exit 1
            ;;
    esac
    shift
done

die () {
    echo "$@"
    exit 1
}

mkdir -p "$dir" || die "Cannot mkdir $dir"
cd "$dir" || die "Cannot chdir to $dir"
wget 'http://sourceforge.net/projects/boost/files/boost/1.49.0/boost_1_49_0.tar.gz/download' -O boost_1_49_0.tar.gz
tar xzvf boost_1_49_0.tar.gz
cd boost_1_49_0/
./bootstrap.sh
./b2
